// Copyright 2022-2023 The Matrix.org Foundation C.I.C.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

//! Decentralised MLS group

use core::cmp::Ordering;
use log::{debug, info};
use openmls::framing::errors::MessageDecryptionError;
use openmls::group::export::MlsGroupExport;
use openmls::messages::group_info::VerifiableGroupInfo;
use openmls::prelude::{
    CreateMessageError,
    CredentialWithKey,
    ErrorString,
    ExportGroupInfoError,
    ExternalCommitError,
    GroupId,
    KeyPackage,
    LeafNode,
    LeafNodeIndex,
    MergeCommitError,
    MlsGroup,
    MlsGroupConfig,
    MlsMessageOut,
    NewGroupError,
    OpenMlsKeyStore,
    ProcessedMessage,
    ProcessMessageError,
    ProtocolMessage,
    ProtocolVersion,
    RatchetTreeIn,
    Sender,
    StagedCommit,
    ValidationError,
    Welcome,
    WelcomeError,
};
use openmls::treesync::RatchetTree;
use openmls_traits::OpenMlsCryptoProvider;
use openmls_traits::signatures::Signer;
use tls_codec::Serialize as TlsSerializeTrait;

use serde::{Serialize, Deserialize};
use std::collections::btree_map::BTreeMap;
use std::collections::btree_set::BTreeSet;
use std::collections::hash_map::HashMap;
use std::collections::HashSet;
use std::vec::Vec;

use crate::errors::DmlsError;
use crate::traits::DmlsCryptoProvider;
use crate::types::DmlsEpoch;

#[derive(Debug, PartialEq, Eq, Clone, Deserialize, Serialize)]
enum MembershipChange {
    Add(u64),
    Remove(u64),
    Update(u64, u64),
}

type MemberDiff = BTreeMap<Vec<u8>, MembershipChange>;

/// A DMLS group
///
/// A `DmlsGroup` manages a DMLS group.  It allows you to add/remove members,
/// resolve forks, decrypt and encrypt messages.
///
/// ## Backend
///
/// The `DmlsGroup` requires a backend to store groups data, perform
/// cryptographic operations, and obtain information from the application.
/// This is done via a struct that implements the
/// [`DmlsCryptoProvider`](crate::traits::DmlsCryptoProvider) trait.
///
/// ## Creating a group
///
/// There are several ways to create a DMLS group:
///
/// - you can create a brand new group using [`DmlsGroup::new`]
/// - you can join a group after receiving an MLS welcome message using
///   [`DmlsGroup::new_from_welcome`]
/// - you can join a group via an external join using
///   [`DmlsGroup::join_by_external_commit`], or
/// - you can create an empty group (for importing group exports) using
///   [`DmlsGroup::new_dummy_group`].
///
/// Due to the way DMLS works, a welcome message may be received for a group
/// that you already have a `DmlsGroup` for.  However, we will not detect that
/// until we have processed the welcome message.  To ensure that you only have
/// one `DmlsGroup` for each group, use [`group_id`](DmlsGroup::group_id) to
/// obtain the group ID, check if there is already an `DmlsGroup` for that ID,
/// and if so, use
/// [`add_epoch_from_new_group`](DmlsGroup::add_epoch_from_new_group) to merge
/// the new group into the existing group.
///
/// Similarly, if you join a group using [`DmlsGroup::join_by_external_commit`]
/// when you previously had a group, for example, created using
/// [`DmlsGroup::new_dummy_group`], use [`add_epoch_from_new_group`](DmlsGroup::add_epoch_from_new_group) to merge the
/// new group into the existing group.
///
/// ## Decrypting messages
///
/// When an MLS-encrypted message is received, it needs to be parsed using
/// [`parse_message`](DmlsGroup::parse_message), giving an
/// [`openmls::prelude::UnverifiedMessage`] that can be inspected,
/// and then processed using
/// [`process_unverified_message`](DmlsGroup::process_unverified_message).
/// This results in an [`openmls::prelude::ProcessedMessage`], which
/// can either represent a staged commit or an application message.
///
/// If it is a staged commit, it can be merged, creating a new epoch (and
/// possibly forking the MLS group), using
/// [`merge_staged_commit`](DmlsGroup::merge_staged_commit).
///
/// ## Tracking membership
///
/// Since each underlying MLS group may have its own group membership, the
/// `DmlsGroup` must keep track of group membership itself.  The `DmlsGroup`
/// begins with the group membership of the group used to create the
/// `DmlsGroup`.
///
/// - For a group created using [`DmlsGroup::new`], the initial group membership
///   is only your own users
/// - For a group created using [`DmlsGroup::new_from_welcome`] or
///   [`DmlsGroup::join_by_external_commit`], the initial group membership is the
///   membership of the group from the welcome message or from the public group
///   state, respectively
/// - For a group created using [`DmlsGroup::new_dummy_group`], the initial group
///   membership will be the group membership that first gets added using
///   [`add_epoch_from_new_group`](DmlsGroup::add_epoch_from_new_group)
///
/// The group membership can be obtained by calling [`members`](DmlsGroup::members).
///
/// When users join or leave, the group membership can be managed by using
/// [`add_member`](DmlsGroup::add_member) and
/// [`remove_member`](DmlsGroup::remove_member).  The membership changes can be
/// applied to the group using [`resolve`](DmlsGroup::resolve), and
/// [`has_changes`](DmlsGroup::has_changes) indicates whether there are any
/// membership changes that need to be applied.
///
/// ## Encrypting messages
///
/// Before encrypting, you need to check if there are membership changes that
/// need to be applied or if there are epoch forks that needs to be resolved by
/// calling [`has_changes`](DmlsGroup::has_changes) and
/// [`needs_resolve`](DmlsGroup::needs_resolve), respectively.  If either
/// returns true, call [`resolve`](DmlsGroup::resolve) to create a new epoch.
///
/// After this, you can call [`encrypt_message`](DmlsGroup::encrypt_message) to
/// encrypt the message.
///
/// ## Persisting the group
///
/// MLS group and key information will be automatically persisted via the
/// backend ([`matrix_dmls::traits::DmlsCryptoProvider`]), but the DMLS group
/// must also be persisted whenever changes are made.  However, this is not
/// done automatically, as applications may want to batch some changes before
/// persisting.  For example, an application may issue several membership
/// changes and then a `resolve` before persisting the DMLS group if they are
/// done in quick succession.  The DMLS group should be persisted after calling:
///
/// - any of the functions that create the DMLS group (unless the new DMLS group
///   is to be merged into another group)
/// - `add_epoch_from_new_group`
/// - `merge_staged_commit`
/// - `resolve`
/// - `add_member`
/// - `remove_member`
#[derive(Debug, Deserialize, Serialize)]
pub struct DmlsGroup {
    group_id: GroupId,
    mls_group_config: MlsGroupConfig,
    extremities: BTreeSet<DmlsEpoch>,
    last_base: Option<DmlsEpoch>,
    identity: Vec<u8>,
    member_changes: MemberDiff, // the changes since the last_base epoch
    member_generations: HashMap<Vec<u8>, (DmlsEpoch, u64, LeafNode)>,
    // the difference in membership between the base epoch and all epochs in extremities
    member_diffs: BTreeMap<DmlsEpoch, MemberDiff>, // FIXME: put this in storage
}

// Generates a membership diff from two sets of key packages
fn membership_diff_from_members(
    mut members1: Vec<&LeafNode>,
    mut members2: Vec<&LeafNode>,
) -> MemberDiff {
    let mut diff: MemberDiff = MemberDiff::new();

    members1.sort_unstable_by_key(|node| node.credential().identity());
    members2.sort_unstable_by_key(|node| node.credential().identity());

    let mut m1_iter = members1.iter();
    let mut m2_iter = members2.iter();

    let mut next_m1 = m1_iter.next();
    let mut next_m2 = m2_iter.next();

    loop {
        match (next_m1, next_m2) {
            (None, None) => break,
            (Some(member1), Some(member2)) => match member1
                .credential()
                .identity()
                .cmp(member2.credential().identity())
            {
                Ordering::Less => {
                    diff.insert(
                        member1.credential().identity().into(),
                        MembershipChange::Remove(get_generation(member1)),
                    );
                    next_m1 = m1_iter.next();
                }
                Ordering::Equal => {
                    diff.insert(
                        member1.credential().identity().into(),
                        MembershipChange::Update(get_generation(member1), get_generation(member2)),
                    );
                    next_m1 = m1_iter.next();
                    next_m2 = m2_iter.next();
                }
                Ordering::Greater => {
                    diff.insert(
                        member2.credential().identity().into(),
                        MembershipChange::Remove(get_generation(member1)),
                    );
                    next_m2 = m2_iter.next();
                }
            },
            (Some(member), None) => {
                diff.insert(
                    member.credential().identity().into(),
                    MembershipChange::Remove(get_generation(member)),
                );
                next_m1 = m1_iter.next();
            }
            (None, Some(member)) => {
                diff.insert(
                    member.credential().identity().into(),
                    MembershipChange::Add(get_generation(member)),
                );
                next_m2 = m2_iter.next();
            }
        }
    }

    diff
}

// Given a membership diff from group A to B, and a diff from group A to C,
// calculate the diff from B to C
fn transform_membership_diff(diff_ab: &MemberDiff, diff_ac: &MemberDiff) -> MemberDiff {
    let mut diff_bc: MemberDiff = MemberDiff::new();
    let mut diff_ab_iter = diff_ab.iter();
    let mut diff_ac_iter = diff_ac.iter();

    let mut next_diff_ab = diff_ab_iter.next();
    let mut next_diff_ac = diff_ac_iter.next();

    loop {
        match (next_diff_ab, next_diff_ac) {
            (None, None) => break,
            (Some((ab_member, ab_change)), Some((ac_member, ac_change))) => match ab_member
                .cmp(ac_member)
            {
                Ordering::Less => {
                    match ab_change {
                        MembershipChange::Add(gen) => {
                            diff_bc.insert(ab_member.clone(), MembershipChange::Remove(*gen))
                        }
                        MembershipChange::Remove(gen) => {
                            diff_bc.insert(ab_member.clone(), MembershipChange::Add(*gen))
                        }
                        MembershipChange::Update(gen1, gen2) => diff_bc
                            .insert(ab_member.clone(), MembershipChange::Update(*gen2, *gen1)),
                    };
                    next_diff_ab = diff_ab_iter.next();
                }
                Ordering::Equal => {
                    match (ab_change, ac_change) {
                        (MembershipChange::Add(gen1), MembershipChange::Add(gen2)) => {
                            if gen1 != gen2 {
                                diff_bc.insert(
                                    ac_member.clone(),
                                    MembershipChange::Update(*gen1, *gen2),
                                );
                            }
                        }
                        (MembershipChange::Add(_), _) => (), // not possible
                        (_, MembershipChange::Add(_)) => (), // not possible
                        (MembershipChange::Remove(_), MembershipChange::Remove(_)) => (),
                        (MembershipChange::Remove(gen1), MembershipChange::Update(gen2, gen3)) => {
                            diff_bc.insert(ac_member.clone(), MembershipChange::Add(*gen3));
                        }
                        (MembershipChange::Update(gen1, gen2), MembershipChange::Remove(gen3)) => {
                            diff_bc.insert(ac_member.clone(), MembershipChange::Remove(*gen2));
                        }
                        (
                            MembershipChange::Update(gen1, gen2),
                            MembershipChange::Update(gen3, gen4),
                        ) => {
                            if gen2 != gen4 {
                                diff_bc.insert(
                                    ac_member.clone(),
                                    MembershipChange::Update(*gen2, *gen4),
                                );
                            }
                        }
                    }
                    next_diff_ab = diff_ab_iter.next();
                    next_diff_ac = diff_ac_iter.next();
                }
                Ordering::Greater => {
                    diff_bc.insert(ac_member.clone(), ac_change.clone());
                    next_diff_ac = diff_ac_iter.next();
                }
            },
            (Some((ab_member, ab_change)), None) => {
                match ab_change {
                    MembershipChange::Add(gen) => {
                        diff_bc.insert(ab_member.clone(), MembershipChange::Remove(*gen))
                    }
                    MembershipChange::Remove(gen) => {
                        diff_bc.insert(ab_member.clone(), MembershipChange::Add(*gen))
                    }
                    MembershipChange::Update(gen1, gen2) => {
                        diff_bc.insert(ab_member.clone(), MembershipChange::Update(*gen2, *gen1))
                    }
                };
                next_diff_ab = diff_ab_iter.next();
            }
            (None, Some((ac_member, ac_change))) => {
                diff_bc.insert(ac_member.clone(), ac_change.clone());
                next_diff_ac = diff_ac_iter.next();
            }
        }
    }

    diff_bc
}

#[doc(hidden)]
// do not use - for internal use and debugging purposes only
pub fn get_generation(node: &LeafNode) -> u64 {
    node.extensions().generation()
        .map_or(0, |e| e.generation())
}

#[derive(tls_codec::TlsSize, tls_codec::TlsSerialize)]
struct GroupStorageKey<'a> {
    group_id: &'a GroupId,
    epoch: &'a DmlsEpoch,
    historical: u8,
}

fn store_group<KeyStore: OpenMlsKeyStore>(
    backend: &mut impl DmlsCryptoProvider<OpenMlsKeyStore = KeyStore>,
    group_id: &GroupId,
    epoch: &DmlsEpoch,
    historical: bool,
    group: &MlsGroup,
) -> Result<(), KeyStore::Error> {
    let key = GroupStorageKey {
        group_id,
        epoch,
        historical: if historical { 1 } else { 0 }
    }
    .tls_serialize_detached()
    .unwrap();
    backend.mls_crypto_provider().key_store().store(&key, group)
}

fn read_group<KeyStore: OpenMlsKeyStore>(
    backend: &mut impl DmlsCryptoProvider<OpenMlsKeyStore = KeyStore>,
    group_id: &GroupId,
    epoch: &DmlsEpoch,
    historical: bool,
) -> Option<MlsGroup> {
    let key = GroupStorageKey {
        group_id,
        epoch,
        historical: if historical { 1 } else { 0 }
    }
    .tls_serialize_detached()
    .unwrap();
    backend.mls_crypto_provider().key_store().read::<MlsGroup>(&key)
}

impl DmlsGroup {
    /// Creates a new DMLS group with only the creator as member
    ///
    /// The parameters are the same as for [`openmls::prelude::MlsGroup::new`],
    /// except that the backend is a [`crate::traits::DmlsCryptoProvider`]
    /// rather than an [`openmls::prelude::OpenMlsCryptoProvider`].
    pub fn new<KeyStore: OpenMlsKeyStore>(
        backend: &mut impl DmlsCryptoProvider<OpenMlsKeyStore = KeyStore>,
        signer: &impl Signer,
        mls_group_config: &MlsGroupConfig,
        group_id: GroupId,
        credential_with_key: CredentialWithKey,
    ) -> Result<Self, NewGroupError<KeyStore::Error>> {
        let group = MlsGroup::new_with_group_id(
            backend.mls_crypto_provider(),
            signer,
            mls_group_config,
            group_id.clone(),
            credential_with_key,
        )?;

        let export = group.export_group().unwrap();
        let historical_group = MlsGroup::create_from_export(
            backend.mls_crypto_provider(),
            export,
            mls_group_config,
        ).unwrap();

        let credential = group
            .credential()
            .or(Err(NewGroupError::NoMatchingKeyPackage))?
            .clone(); // FIXME:
        let identity = credential.identity();

        let epoch = DmlsEpoch::new(group.epoch(), identity);

        let extremities = BTreeSet::from([epoch.clone()]);

        store_group(backend, &group_id, &epoch, false, &group);
        store_group(backend, &group_id, &epoch, true, &historical_group);

        let dmls_group = DmlsGroup {
            group_id,
            mls_group_config: mls_group_config.clone(),
            extremities,
            member_changes: BTreeMap::new(),
            last_base: Some(epoch),
            identity: identity.to_vec(),
            member_generations: HashMap::new(),
            member_diffs: BTreeMap::new(),
        };

        Ok(dmls_group)
    }

    /// Creates a new DMLS group from a [`Welcome`] message
    ///
    /// The arguments are the same as for
    /// [`openmls::prelude::MlsGroup::new_from_welcome`], except that the
    /// backend is a [`crate::traits::DmlsCryptoProvider`] rather than an
    /// [`openmls::prelude::OpenMlsCryptoProvider`], and with the addition of a
    /// sender parameter, which is the identity of the member who created the
    /// [`Welcome`] message (the member who created the commit).
    pub fn new_from_welcome<KeyStore: OpenMlsKeyStore>(
        backend: &mut impl DmlsCryptoProvider<OpenMlsKeyStore = KeyStore>,
        mls_group_config: &MlsGroupConfig,
        welcome: Welcome,
        ratchet_tree: Option<RatchetTreeIn>,
        sender: &[u8],
    ) -> Result<Self, WelcomeError<KeyStore::Error>> {
        let group = MlsGroup::new_from_welcome(
            backend.mls_crypto_provider(),
            mls_group_config,
            welcome,
            ratchet_tree,
        )?;

        let export = group.export_group().unwrap();
        let historical_group = MlsGroup::create_from_export(
            backend.mls_crypto_provider(),
            export,
            mls_group_config,
        ).unwrap();

        let group_id = group.group_id().clone();
        let credential = group
            .credential()
            .or(Err(WelcomeError::NoMatchingKeyPackage))? // FIXME:
            .clone();
        let identity = credential.identity();

        let epoch = DmlsEpoch::new(group.epoch(), sender);

        debug!("New group from welcome message with epoch {:?}", epoch);

        let extremities = BTreeSet::from([epoch.clone()]);

        store_group(backend, &group_id, &epoch, false, &group);
        store_group(backend, &group_id, &epoch, true, &historical_group);

        let mut member_generations: HashMap<Vec<u8>, (DmlsEpoch, u64, LeafNode)> =
            HashMap::new();

        for leaf_node in group.leaf_nodes() {
            if let Some(leaf_node) = leaf_node {
                let identity = Vec::from(leaf_node.credential().identity());
                let generation = get_generation(leaf_node);
                member_generations.insert(
                    identity,
                    (
                        epoch.clone(),
                        generation,
                        leaf_node.clone(),
                    ),
                );
            }
        }

        let dmls_group = DmlsGroup {
            group_id,
            mls_group_config: mls_group_config.clone(),
            extremities,
            member_changes: BTreeMap::new(),
            last_base: Some(epoch),
            identity: identity.to_vec(),
            member_generations,
            member_diffs: BTreeMap::new(),
        };

        Ok(dmls_group)
    }

    /// Joins a DMLS group by creating an external commit
    ///
    /// The arguments are the same as for
    /// [`openmls::prelude::MlsGroup::join_by_external_commit`], except that
    /// the backend is a [`crate::traits::DmlsCryptoProvider`] rather than an
    /// [`openmls::prelude::OpenMlsCryptoProvider`].
    ///
    /// On success, returns a `DmlsGroup` and a commit message to send to the group.
    pub fn join_by_external_commit(
        backend: &mut impl DmlsCryptoProvider,
        signer: &impl Signer,
        ratchet_tree: Option<RatchetTreeIn>,
        verifiable_group_info: VerifiableGroupInfo,
        mls_group_config: &MlsGroupConfig,
        aad: &[u8],
        credential_with_key: CredentialWithKey,
    ) -> Result<(Self, MlsMessageOut), ExternalCommitError> {
        let identity = credential_with_key.credential.identity().to_vec();

        let (mut group, msg, group_info) = MlsGroup::join_by_external_commit(
            backend.mls_crypto_provider(),
            signer,
            ratchet_tree,
            verifiable_group_info,
            mls_group_config,
            aad,
            credential_with_key,
        )?;

        let export = group.export_group().unwrap();
        let historical_group = MlsGroup::create_from_export(
            backend.mls_crypto_provider(),
            export,
            mls_group_config,
        ).unwrap();

        let group_id = group.group_id().clone();

        group
            .merge_pending_commit(backend.mls_crypto_provider())
            .expect("error merging pending commit");

        let epoch = DmlsEpoch::new(group.epoch(), identity.as_ref());

        debug!("Joining group by external commit with epoch {:?}", epoch);

        let extremities = BTreeSet::from([epoch.clone()]);

        store_group(backend, &group_id, &epoch, false, &group);
        store_group(backend, &group_id, &epoch, true, &historical_group);

        let mut member_generations: HashMap<Vec<u8>, (DmlsEpoch, u64, LeafNode)> =
            HashMap::new();

        for leaf_node in group.leaf_nodes() {
            if let Some(leaf_node) = leaf_node {
                let identity = Vec::from(leaf_node.credential().identity());
                let generation = get_generation(leaf_node);
                member_generations.insert(
                    identity,
                    (
                        epoch.clone(),
                        generation,
                        leaf_node.clone(),
                    ),
                );
            }
        }

        let dmls_group = DmlsGroup {
            group_id,
            mls_group_config: mls_group_config.clone(),
            extremities,
            member_changes: BTreeMap::new(),
            last_base: Some(epoch),
            identity: identity.to_vec(),
            member_generations,
            member_diffs: BTreeMap::new(),
        };

        Ok((dmls_group, msg))
    }

    /// Creates a dummy group to allow importing group state to read old
    /// messages.
    ///
    /// The group cannot encrypt and cannot decrypt handshake messages until it
    /// is joined via a Welcome message or an external join.
    pub fn new_dummy_group(
        backend: &mut impl DmlsCryptoProvider,
        mls_group_config: &MlsGroupConfig,
        group_id: GroupId,
        identity: &[u8],
    ) -> Self {
        DmlsGroup {
            group_id,
            mls_group_config: mls_group_config.clone(),
            extremities: BTreeSet::new(),
            member_changes: BTreeMap::new(),
            last_base: None,
            identity: identity.to_vec(),
            member_generations: HashMap::new(),
            member_diffs: BTreeMap::new(),
        }
    }

    /// Add an epoch from a newly created group from a [`Welcome`] message
    ///
    /// If you received a [`Welcome`] message and it has the same group ID
    /// as a group that you are already in, use this function to add
    /// the group to your already-existing group.
    pub fn add_epoch_from_new_group(
        &mut self,
        backend: &mut impl DmlsCryptoProvider,
        other: DmlsGroup,
        resolves: &[DmlsEpoch],
    ) -> Result<(), String> {
        if other.group_id != self.group_id {
            // FIXME: use a real error class
            return Err("Trying to add epoch from a different group".to_string());
        }
        // FIXME: make sure group parameters (e.g. ciphersuite, MLS version,
        // group extensions) are the same

        if other.extremities.len() != 1 {
            return Err("Group was not a new group".to_string());
        }

        let new_epoch = if let Some(base) = other.last_base {
            base
        } else {
            return Err("Group was not a new group".to_string());
        };

        let group = read_group(backend, &self.group_id, &new_epoch, false)
            .expect("Internal error: cannot find base epoch");

        debug!("Adding epoch {:?} to group from new group", new_epoch);

        store_group(backend, &self.group_id, &new_epoch, false, &group);

        // FIXME: factor this out into a function because this is shared with
        // merge_staged_commit
        for epoch in resolves {
            self.extremities.remove(&epoch);
            self.member_diffs.remove(&epoch);
        }
        match self.last_base.as_ref() {
            None => {
                self.last_base = Some(new_epoch.clone());
            }
            Some(last_base) => {
                let last_base_group = read_group(backend, &self.group_id, last_base, false)
                    .expect("Internal error: cannot find base epoch");
                let changes = membership_diff_from_members(
                    last_base_group.leaf_nodes().filter_map(std::convert::identity).collect(),
                    group.leaf_nodes().filter_map(std::convert::identity).collect(),
                );

                for leaf_node in group.leaf_nodes() {
                    if let Some(leaf_node) = leaf_node {
                        let identity = Vec::from(leaf_node.credential().identity());
                        let generation = get_generation(leaf_node);
                        match self.member_generations.get(&identity) {
                            Some((_, g, _)) if g >= &generation => (),
                            _ => {
                                self.member_generations.insert(
                                    identity,
                                    (
                                        new_epoch.clone(),
                                        generation,
                                        leaf_node.clone(),
                                    ),
                                );
                            }
                        }
                    }
                }

                if resolves.contains(last_base) {
                    // if last_base is the same as from_epoch or in resolves, use this
                    // as the new base and recalculate diffs based on this diff
                    for (_epoch, diff) in self.member_diffs.iter_mut() {
                        *diff = transform_membership_diff(&changes, diff);
                    }
                    self.last_base = Some(new_epoch.clone());
                    self.member_changes = MemberDiff::from_iter(
                        transform_membership_diff(&changes, &self.member_changes)
                            .into_iter()
                            .filter(|(_user, diff)| {
                                // we remove updates because we don't need to track those
                                // for member_changes
                                match diff {
                                    MembershipChange::Update(..) => false,
                                    _ => true,
                                }
                            })
                    );
                } else {
                    self.member_diffs.insert(new_epoch.clone(), changes);
                }
            }
        }
        self.extremities.insert(new_epoch);

        Ok(())
    }

    /// Get the group ID of the group
    pub fn group_id(&self) -> &GroupId {
        &self.group_id
    }

    /// Process an encrypted message
    pub fn process_message(
        &self,
        backend: &mut impl DmlsCryptoProvider,
        message: impl Into<ProtocolMessage>,
        sender: &[u8],
    ) -> Result<ProcessedMessage, DmlsError<ProcessMessageError>> {
        println!("Processing");
        let message = message.into();
        let epoch = DmlsEpoch::new(message.epoch(), sender);
        let is_handshake = message.is_handshake_message();
        let group_result = read_group(backend, &self.group_id, &epoch, false);
        if let Some(mut group) = group_result {
            let result = group.process_message(
                backend.mls_crypto_provider(),
                message.clone(),
            );
            store_group(backend, &self.group_id, &epoch, false, &group);
            if is_handshake {
                result.map_err(|e| DmlsError::WrappedError(e))
            } else if let Err(
                ProcessMessageError::ValidationError(ValidationError::UnableToDecrypt(MessageDecryptionError::GenerationOutOfBound))
            ) = result {
                let mut group = read_group(backend, &self.group_id, &epoch, true)
                    .ok_or(DmlsError::WrappedError(ProcessMessageError::ValidationError(ValidationError::UnableToDecrypt(MessageDecryptionError::GenerationOutOfBound))))?;
                let result = group.process_message(
                    backend.mls_crypto_provider(),
                    message,
                );
                result.map_err(|e| DmlsError::WrappedError(e))
            } else {
                result.map_err(|e| DmlsError::WrappedError(e))
            }
        } else {
                let mut group = read_group(backend, &self.group_id, &epoch, true)
                    .ok_or(DmlsError::EpochNotFound)?;
                let result = group.process_message(backend.mls_crypto_provider(), message);
                result.map_err(|e| DmlsError::WrappedError(e))
        }
    }

    /// Merge a staged commit
    ///
    /// If a received message was a commit message, the application can
    /// inspect the commit to ensure that it is valid, and then use this function
    /// to merge it.
    pub fn merge_staged_commit<KeyStore: OpenMlsKeyStore>(
        &mut self,
        backend: &mut impl DmlsCryptoProvider<OpenMlsKeyStore = KeyStore>,
        staged_commit: StagedCommit,
        from_epoch: &DmlsEpoch,
        sender: &[u8],
        resolves: &[DmlsEpoch],
    ) -> Result<DmlsEpoch, DmlsError<MergeCommitError<KeyStore::Error>>> {
        let mut group = read_group(backend, &self.group_id, from_epoch, false)
            .ok_or(DmlsError::EpochNotFound)?;

        let mut member_generation_updates: Vec<(Vec<u8>, u64, LeafNode)> = Vec::new();

        let last_base = if let Some(base) = self.last_base.as_ref() {
            base
        } else {
            return Err(DmlsError::EpochNotFound);
        };

        // calculate the changes relative to our base
        let mut changes: MemberDiff = self.member_diffs.get(from_epoch).map_or_else(
            || {
                let last_base_group = read_group(backend, &self.group_id, last_base, false)
                    .expect("Internal error: cannot find base epoch");
                membership_diff_from_members(
                    last_base_group.leaf_nodes().filter_map(std::convert::identity).collect(),
                    group.leaf_nodes().filter_map(std::convert::identity).collect(),
                )
            },
            |c| c.clone(),
        );
        for proposal in staged_commit.add_proposals() {
            let leaf_node = proposal.add_proposal().key_package().leaf_node();
            let identity = Vec::from(leaf_node.credential().identity());
            let generation = get_generation(leaf_node);
            match self.member_generations.get(&identity) {
                Some((_, g, _)) if g >= &generation => (),
                _ => member_generation_updates.push((
                    identity.clone(),
                    generation,
                    leaf_node.clone(),
                )),
            }
            changes.insert(identity, MembershipChange::Add(get_generation(leaf_node)));
        }
        for proposal in staged_commit.remove_proposals() {
            let removed_idx = proposal.remove_proposal().removed();
            let removed_leaf = group.leaf(removed_idx)
                .expect("Trying to remove nonexistent member"); // FIXME:
            let removed_member = removed_leaf.credential().identity();
            changes.insert(
                Vec::from(removed_member),
                MembershipChange::Remove(get_generation(removed_leaf)),
            );
        }
        for proposal in staged_commit.update_proposals() {
            match proposal.sender() {
                Sender::Member(idx) => {
                    let orig_leaf = group.leaf(*idx).expect("FIXME:");
                    let new_leaf = proposal.update_proposal().leaf_node();
                    // FIXME: make sure that the key packages belong to the same user
                    let identity = Vec::from(new_leaf.credential().identity());
                    let generation = get_generation(new_leaf);
                    match self.member_generations.get(&identity) {
                        Some((_, g, _)) if g >= &generation => (),
                        _ => member_generation_updates.push((
                            identity.clone(),
                            generation,
                            new_leaf.clone(),
                        )),
                    }
                    changes.insert(
                        identity,
                        MembershipChange::Update(get_generation(orig_leaf), get_generation(new_leaf)),
                    );
                }
                _ => {
                    // FIXME: non-member updates?
                }
            }
        }
        for proposal in staged_commit.update_other_proposals() {
            let (idx, new_leaf) = proposal.update_other_proposal().updated();
            let orig_leaf = group.leaf(*idx).expect("FIXME:");
            // FIXME: make sure that the key packages belong to the same user
            let identity = Vec::from(new_leaf.credential().identity());
            let generation = get_generation(new_leaf);
            match self.member_generations.get(&identity) {
                Some((_, g, _)) if g >= &generation => (),
                _ => member_generation_updates.push((
                    identity.clone(),
                    generation,
                    new_leaf.clone(),
                )),
            }
            changes.insert(
                identity,
                MembershipChange::Update(get_generation(orig_leaf), get_generation(new_leaf)),
            );
        }

        group.merge_staged_commit(backend.mls_crypto_provider(), staged_commit)
            .map_err(|e| DmlsError::WrappedError(e))?;
        let new_epoch = DmlsEpoch::new(group.epoch(), sender);

        member_generation_updates
            .iter()
            .for_each(|(identity, generation, kp_ref)| {
                self.member_generations.insert(
                    identity.clone(),
                    (new_epoch.clone(), *generation, kp_ref.clone()),
                );
            });

        store_group(backend, &self.group_id, &new_epoch, false, &group);
        debug!("New epoch {:?}", new_epoch);

        let _ = group.export_group()
            .map(|export| {
                // if we were removed from the group, group export won't work
                let mut historical_group = MlsGroup::create_from_export(
                    backend.mls_crypto_provider(),
                    export,
                    group.configuration(),
                ).unwrap();

                store_group(backend, &self.group_id, &new_epoch, true, &historical_group);
            });

        // update record keeping
        self.extremities.remove(&from_epoch);
        for epoch in resolves {
            self.extremities.remove(&epoch);
            self.member_diffs.remove(&epoch);
        }
        if !group.is_active() {
            // we've been removed.  If our last_base and all our extremities
            // are resolved, drop the last_base and member_changes as we're out
            // of the group now.  Otherwise, pick one of our remaining
            // extremities as our last_base.
            if resolves.contains(last_base) || last_base == from_epoch {
                match self.extremities.pop_first() {
                    None => {
                        self.last_base = None;
                    },
                    Some(epoch) => {
                        let changes_to_new_base = self.member_diffs.remove(&epoch)
                            .expect("Cannot get diffs from epoch");
                        for (_epoch, diff) in self.member_diffs.iter_mut() {
                            *diff = transform_membership_diff(&changes_to_new_base, diff);
                        }
                        self.member_changes = MemberDiff::from_iter(
                            transform_membership_diff(&changes_to_new_base, &self.member_changes)
                                .into_iter()
                                .filter(|(_user, diff)| {
                                    // we remove updates because we don't need to track those
                                    // for member_changes
                                    match diff {
                                        MembershipChange::Update(..) => false,
                                        _ => true,
                                    }
                                })
                        );
                        self.last_base = Some(epoch);
                    }
                }
            }
        } else if resolves.contains(last_base) || last_base == from_epoch {
            // if last_base is the same as from_epoch or in resolves, use this
            // as the new base and recalculate diffs based on this diff
            for (_epoch, diff) in self.member_diffs.iter_mut() {
                *diff = transform_membership_diff(&changes, diff);
            }
            self.last_base = Some(new_epoch.clone());
            self.member_changes = MemberDiff::from_iter(
                transform_membership_diff(&changes, &self.member_changes)
                    .into_iter()
                    .filter(|(_user, diff)| {
                        // we remove updates because we don't need to track those
                        // for member_changes
                        match diff {
                            MembershipChange::Update(..) => false,
                            _ => true,
                        }
                    })
            );
        } else {
            self.member_diffs.insert(new_epoch.clone(), changes);
        }
        self.extremities.insert(new_epoch.clone());
        Ok(new_epoch)
    }

    /// Note that a member should be added to the group
    ///
    /// The member will be added when the next call to
    /// [`resolve`](Self::resolve) is made.
    pub fn add_member(&mut self, backend: &mut impl DmlsCryptoProvider, member: &[u8]) -> () {
        let member_vec = Vec::from(member);
        let last_base = self.last_base.as_ref().unwrap(); // FIXME:
        match self.member_changes.get(&member_vec) {
            None => {
                let group = read_group(backend, &self.group_id, last_base, false)
                    .expect("Internal error: cannot find base epoch");
                let members = group.leaf_nodes();
                members
                    .filter_map(std::convert::identity)
                    // find the member's key package
                    .find_map(|leaf| {
                        if leaf.credential().identity() == member {
                            Some(())
                        } else {
                            None
                        }
                    })
                    // if the member is found, do nothing.  If not found,
                    // add them
                    .or_else(|| {
                        self.member_changes
                            .insert(member_vec, MembershipChange::Add(0));
                        None
                    });
            }
            Some(MembershipChange::Remove(_)) => {
                // member was previously marked for removal, so just unmark them
                self.member_changes.remove(&member_vec);
            }
            _ => {} // they're already marked as added, so do nothing
        }
    }

    /// Note that a member should be removed from the group
    ///
    /// The member will be removed when the next call to
    /// [`resolve`](Self::resolve) is made.
    pub fn remove_member(&mut self, backend: &mut impl DmlsCryptoProvider, member: &[u8]) -> () {
        let member_vec = Vec::from(member);
        let last_base = self.last_base.as_ref().unwrap(); // FIXME:
        match self.member_changes.get(&member_vec) {
            None => {
                let group = read_group(backend, &self.group_id, last_base, false)
                    .expect("Internal error: cannot find base epoch");
                let members = group.leaf_nodes();
                members
                    .filter_map(std::convert::identity)
                    // find the member's key package and extract the generation number
                    .find_map(|leaf| {
                        if leaf.credential().identity() == member {
                            Some(get_generation(leaf))
                        } else {
                            None
                        }
                    })
                    // if the member is found, record the removal.  If not found,
                    // don't do anything
                    .map(|gen| {
                        self.member_changes
                            .insert(Vec::from(member), MembershipChange::Remove(gen))
                    });
            }
            Some(MembershipChange::Add(_)) => {
                // we marked them as added, but they're not in the group yet, so
                // just unmark them
                self.member_changes.remove(&member_vec);
            }
            _ => {} // they're already marked for removal, so do nothing
        }
    }

    /// Create a new commit, resolving any forks and applying any membership changes
    ///
    /// Returns
    /// - a commit message to send to the group,
    /// - the DMLS epoch that the commit is based on,
    /// - a list of DMLS epochs that the commit resolves, and
    /// - if there are members than need a Welcome message:
    ///   - the [`Welcome`] message to send
    ///   - the ratchet tree to send (if the group does not use the
    ///     `RatchetTree` extension)
    ///   - a list of users to send to
    pub async fn resolve(
        &mut self,
        backend: &mut impl DmlsCryptoProvider,
        signer: &impl Signer,
    ) -> Result<
        (
            MlsMessageOut,
            DmlsEpoch,
            Vec<DmlsEpoch>,
            Option<(Welcome, RatchetTree, Vec<Vec<u8>>)>,
        ),
        DmlsError<String>,
    > {
        let mut resolves: Vec<DmlsEpoch> =
            self.extremities.iter().map(|epoch| epoch.clone()).collect();
        let last_base = if let Some(base) = self.last_base.as_ref() {
            base
        } else {
            return Err(DmlsError::EpochNotFound);
        };
        let base_epoch = resolves
            .pop()
            .expect("Internal error: extremities is empty");
        let mut base_group = read_group(backend, &self.group_id, &base_epoch, false)
            .expect("Internal error: cannot find base epoch");
        let empty_map: MemberDiff = MemberDiff::new();

        // how does our new base epoch differ from our previous base epoch?
        let diff_from_old_base: &MemberDiff = if last_base == &base_epoch {
            &empty_map
        } else {
            self.member_diffs
                .get(&base_epoch)
                .expect("Internal error: cannot find membership diff")
        };

        // how do the requested membership changes from the old base
        // translate to changes to the new base?
        let diff = transform_membership_diff(diff_from_old_base, &self.member_changes);

        // FIXME: apply changes to new base
        let mut removes: Vec<LeafNodeIndex> = Vec::new();
        let mut updates: Vec<(LeafNodeIndex, LeafNode)> = Vec::new();

        let members = base_group.members();
        let member_indexes_by_id: HashMap<Vec<u8>, LeafNodeIndex> = members
            .map(|member| {
                (
                    Vec::from(member.credential.identity()),
                    member.index,
                )
            })
            .collect();

        let mut to_add: Vec<&[u8]> = Vec::new();
        let mut kps_to_add: Vec<KeyPackage> = Vec::new();

        diff.iter().for_each(|(id, change)| {
            match change {
                MembershipChange::Add(_) => self.member_generations.get(id).map_or_else(
                    || {
                        to_add.push(id.as_slice());
                    },
                    |(epoch, _, leaf_node)| {
                        let kp = KeyPackage::new_reused_from_leaf_node(
                            self.mls_group_config.crypto_config(),
                            &self.group_id,
                            leaf_node,
                        )
                            .expect("Unable to create key package");
                        kps_to_add.push(kp);
                    },
                ),
                MembershipChange::Remove(_) => {
                    removes.push(*member_indexes_by_id.get(id).expect(""));
                }
                MembershipChange::Update(_, _) => {
                    // ignore any updates
                }
            }
        });

        let mut adds: Vec<KeyPackage> = Vec::from_iter(
            backend
                .get_init_keys(to_add.as_slice())
                .await
                .iter()
                .filter_map(
                    |k_opt| k_opt.as_ref().map(
                        |k| k.clone().validate(
                            backend.mls_crypto_provider().crypto(),
                            ProtocolVersion::default(),
                        ).ok()
                    ).flatten()
                ),
        );
        adds.append(&mut kps_to_add);

        // check generations from other epochs and update to latest
        let leaf_nodes_vec = base_group.leaf_nodes()
            .collect::<Vec<_>>();
        let mut leaf_nodes = leaf_nodes_vec
            .iter()
            .enumerate()
            .filter_map(|(idx, leaf_opt)| leaf_opt.map(|leaf| (idx, leaf)));
        let mut generations_iter = self.member_generations.iter();
        let mut next_leaf = leaf_nodes.next();
        let mut next_member_id_opt = next_leaf.map(|(_, leaf)| leaf.credential().identity());
        let mut next_generation = generations_iter.next();
        loop {
            match (next_member_id_opt, next_generation) {
                (
                    Some(next_member_id),
                    Some((next_generation_member_id, (epoch, generation, _))),
                ) => {
                    match next_member_id.cmp(next_generation_member_id) {
                        Ordering::Less => {
                            next_leaf = leaf_nodes.next();
                            next_member_id_opt =
                                next_leaf.map(|(_, leaf)| leaf.credential().identity());
                        }
                        Ordering::Greater => {
                            next_generation = generations_iter.next();
                        }
                        Ordering::Equal => {
                            let (idx, leaf) = next_leaf.unwrap();
                            if generation > &get_generation(leaf) {
                                updates.push((LeafNodeIndex::new(idx as u32), leaf.clone()));
                            }
                            next_leaf = leaf_nodes.next();
                            next_member_id_opt =
                                next_leaf.map(|(_, leaf)| leaf.credential().identity());
                            next_generation = generations_iter.next();
                        }
                    };
                }
                _ => break,
            }
        }

        // apply updates
        let (message_out, welcome_option) = base_group
            .create_epoch(
                backend.mls_crypto_provider(),
                signer,
                adds.as_slice(),
                removes.as_slice(),
                updates.as_slice(),
            )
            .map_err(|error| DmlsError::WrappedError(format!("{error:?}")))?;
        base_group
            .merge_pending_commit(backend.mls_crypto_provider())
            .expect("error merging pending commit");

        let new_epoch = DmlsEpoch::new(base_group.epoch(), self.identity.as_slice());

        for key_package in &adds {
            let leaf_node = key_package.leaf_node();
            let identity = Vec::from(leaf_node.credential().identity());
            let generation = get_generation(leaf_node);
            self.member_generations.insert(
                identity,
                (
                    new_epoch.clone(),
                    generation,
                    leaf_node.clone(),
                ),
            );
        }

        debug!("Created new epoch {:?}", new_epoch);

        let export = base_group.export_group().unwrap();
        let mut historical_group = MlsGroup::create_from_export(
            backend.mls_crypto_provider(),
            export,
            &self.mls_group_config,
        ).unwrap();

        store_group(backend, &self.group_id, &new_epoch, false, &base_group);
        store_group(backend, &self.group_id, &new_epoch, true, &historical_group);
        self.extremities = BTreeSet::from([new_epoch.clone()]);
        // FIXME: re-add members who failed to be added
        self.member_changes = BTreeMap::new();
        self.member_diffs = BTreeMap::new();
        self.last_base = Some(new_epoch);

        Ok((
            message_out,
            base_epoch,
            resolves,
            welcome_option.map(|welcome| {
                (
                    welcome,
                    base_group.export_ratchet_tree(),
                    Vec::from_iter(adds.iter().map(
                        |kp| Vec::from(kp.leaf_node().credential().identity())
                    )),
                )
            }),
        ))
    }

    /// Encrypt an application message
    ///
    /// Returns [`DmlsError::EpochNotFound`] if there are any unresolved
    /// extremities.
    pub fn encrypt_message(
        &mut self,
        backend: &mut impl DmlsCryptoProvider,
        signer: &impl Signer,
        message: &[u8],
    ) -> Result<(MlsMessageOut, DmlsEpoch), DmlsError<CreateMessageError>> {
        if self.extremities.len() != 1 {
            Err(DmlsError::EpochNotFound) // FIXME:
        } else {
            let last_base = self.last_base.as_ref().unwrap(); // if there are extremities, then last_base cannot be None
            let mut group = read_group(backend, &self.group_id, last_base, false)
                .ok_or(DmlsError::EpochNotFound)?;
            let result = group.create_message(backend.mls_crypto_provider(), signer, message);
            store_group(backend, &self.group_id, last_base, false, &group);
            result.map_or_else(
                |e| Err(DmlsError::WrappedError(e)),
                |m| Ok((m, last_base.clone())),
            )
        }
    }

    #[doc(hidden)]
    // for debugging purposes only
    pub fn group(
        &self,
        backend: &mut impl DmlsCryptoProvider,
    ) -> Result<MlsGroup, DmlsError<CreateMessageError>> {
        if self.extremities.len() != 1 {
            Err(DmlsError::EpochNotFound) // FIXME:
        } else {
            let last_base = self.last_base.as_ref().unwrap(); // if there are extremities, then last_base cannot be None
            let group = read_group(backend, &self.group_id, last_base, false)
                .ok_or(DmlsError::EpochNotFound)?;
            Ok(group)
        }
    }

    /// Get the public group state for external joins
    pub fn public_group_info(
        &self,
        backend: &mut impl DmlsCryptoProvider,
        signer: &impl Signer,
        with_ratchet_tree: bool,
    ) -> Result<MlsMessageOut, DmlsError<ExportGroupInfoError>> {
        if self.extremities.len() != 1 {
            Err(DmlsError::EpochNotFound) // FIXME:
        } else {
            let last_base = self.last_base.as_ref().unwrap(); // if there are extremities, then last_base cannot be None
            let group = read_group(backend, &self.group_id, last_base, false)
                .ok_or(DmlsError::EpochNotFound)?;
            let public_group_info = group.export_group_info(backend.mls_crypto_provider(), signer, with_ratchet_tree)
                .map_err(|e| DmlsError::WrappedError(e))?;
            Ok(public_group_info)
        }
    }

    /// Export the ratchet tree
    pub fn export_ratchet_tree(
        &self,
        backend: &mut impl DmlsCryptoProvider,
    ) -> Result<RatchetTree, DmlsError<ExportGroupInfoError>> {
        if self.extremities.len() != 1 {
            Err(DmlsError::EpochNotFound) // FIXME:
        } else {
            let last_base = self.last_base.as_ref().unwrap(); // if there are extremities, then last_base cannot be None
            let group = read_group(backend, &self.group_id, last_base, false)
                .ok_or(DmlsError::EpochNotFound)?;
            Ok(group.export_ratchet_tree())
        }
    }

    /// Get the current epoch
    ///
    /// Returns [`DmlsError::EpochNotFound`] if there are any unresolved
    /// extremities.
    pub fn epoch(&self) -> Result<&DmlsEpoch, DmlsError<()>> {
        if self.extremities.len() != 1 {
            Err(DmlsError::EpochNotFound) // FIXME:
        } else {
            let last_base = self.last_base.as_ref().unwrap(); // if there are extremities, then last_base cannot be None
            Ok(last_base)
        }
    }

    /// Export a group for key backup
    pub fn export_group(
        &self,
        backend: &mut impl DmlsCryptoProvider,
        epoch: &DmlsEpoch,
    ) -> Result<MlsGroupExport, DmlsError<()>> {
        let group = read_group(backend, &self.group_id, epoch, true)
            .ok_or(DmlsError::EpochNotFound)?;
        group.export_group()
            .map_err(|e| DmlsError::WrappedError(e))
    }

    /// Import a previously-exported group
    pub fn import_group(
        &self,
        backend: &mut impl DmlsCryptoProvider,
        epoch: &DmlsEpoch,
        export: MlsGroupExport,
    ) -> Result<(), DmlsError<ErrorString>> {
        let mut group = MlsGroup::create_from_export(
            backend.mls_crypto_provider(),
            export,
            &self.mls_group_config,
        )
            .map_err(|e| DmlsError::WrappedError(e))?;
        if group.epoch() != epoch.mls_epoch() {
            Err(DmlsError::EpochNotFound)
        } else {
            store_group(backend, &self.group_id, epoch, true, &group);
            Ok(())
        }
    }

    /// Indicates whether the group has any forks that need resolving
    pub fn needs_resolve(&self) -> bool {
        self.extremities.len() != 1
    }

    /// Indicates whether there are any membership changes that resolving
    pub fn has_changes(&self) -> bool {
        !self.member_changes.is_empty()
    }

    /// Get the current group members
    pub fn members(
        &self,
        backend: &mut impl DmlsCryptoProvider,
    ) -> HashSet<Vec<u8>> {
        let last_base = self.last_base.as_ref().unwrap(); // FIXME:
        let base_group = read_group(backend, &self.group_id, last_base, false)
            .expect("Internal error: cannot find base epoch");
        let mut members: HashSet<Vec<u8>> = HashSet::new();

        for member in base_group.members() {
            let identity = Vec::from(member.credential.identity());
            members.insert(identity);
        }

        for (identity, change) in self.member_changes.iter() {
            match change {
                MembershipChange::Add(..) => { members.insert(identity.clone()); },
                MembershipChange::Remove(..) => { members.remove(identity); },
                _ => {},
            }
        }

        members
    }

    /// Returns whether the user is joined.
    ///
    /// The user may not be joined if the group was created using
    /// `new_dummy_group`, or if the user was removed from the group.
    pub fn is_joined(&self) -> bool {
        self.last_base.is_some()
    }
}
