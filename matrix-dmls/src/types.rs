// Copyright 2022-2023 The Matrix.org Foundation C.I.C.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

use core::cmp::Ordering;
use openmls::prelude::GroupEpoch;
use serde::{Serialize, Deserialize};
use std::vec::Vec;

/// An epoch for DMLS
///
/// In DMLS, and epoch consists of an MLS epoch and the identifier for the user
/// who created the epoch.
#[derive(Clone, Debug, Deserialize, Serialize, tls_codec::TlsSize, tls_codec::TlsSerialize)]
pub struct DmlsEpoch {
    mls_epoch: GroupEpoch,
    creator: Vec<u8>,
}

impl DmlsEpoch {
    pub fn new(mls_epoch: GroupEpoch, creator: &[u8]) -> Self {
        DmlsEpoch {
            mls_epoch,
            creator: creator.to_vec(),
        }
    }

    pub fn mls_epoch(&self) -> GroupEpoch {
        self.mls_epoch
    }

    pub fn creator<'a>(&'a self) -> &'a Vec<u8> {
        &self.creator
    }
}

impl PartialEq for DmlsEpoch {
    fn eq(&self, other: &Self) -> bool {
        self.mls_epoch.as_u64() == other.mls_epoch.as_u64() && self.creator == other.creator
    }
}

impl Eq for DmlsEpoch {}

impl Ord for DmlsEpoch {
    fn cmp(&self, other: &Self) -> Ordering {
        self.mls_epoch
            .as_u64()
            .cmp(&other.mls_epoch.as_u64())
            .then_with(|| self.creator.cmp(&other.creator))
    }
}

impl PartialOrd for DmlsEpoch {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
