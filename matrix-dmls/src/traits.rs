// Copyright 2022-2023 The Matrix.org Foundation C.I.C.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

use openmls::prelude::{KeyPackageIn, OpenMlsCryptoProvider, OpenMlsKeyStore};

/// Application-defined backend for various functionality.
///
/// An implementation of this must be passed to DMLS functions to provide
/// cryptographic functions to the OpenMLS layer, store groups, and retrieve
/// init keys for users.
pub trait DmlsCryptoProvider {
    type KeyStoreError: std::error::Error + std::fmt::Debug + PartialEq;
    type OpenMlsKeyStore: OpenMlsKeyStore<Error = Self::KeyStoreError>;
    type OpenMlsCryptoProvider: OpenMlsCryptoProvider<KeyStoreProvider = Self::OpenMlsKeyStore>;
    /// Return an [`OpenMlsCryptoProvider`].
    ///
    /// Can use
    /// [`openmls_rust_crypto::RustCrypto`](https://docs.rs/openmls_rust_crypto/latest/openmls_rust_crypto/struct.RustCrypto.html)
    /// for the `parts` and `rand` parts.
    fn mls_crypto_provider(&mut self) -> &mut Self::OpenMlsCryptoProvider;
    /// Get init key KeyPackages for the given users.
    ///
    /// The return value should be a vector of
    /// [`openmls::prelude::KeyPackage`]s, with the [`KeyPackage`] for a user
    /// is in the same position in the vector as the user's ID was in the input
    /// vector.  If no [`KeyPackage`] for a user could be obtained, the value
    /// for that user should be `None`.
    async fn get_init_keys(&mut self, users: &[&[u8]]) -> Vec<Option<KeyPackageIn>>;
}
