// Copyright 2022-2023 The Matrix.org Foundation C.I.C.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#![feature(async_fn_in_trait)]

use matrix_dmls::dmls_group::{get_generation, DmlsGroup};
use matrix_dmls::traits::DmlsCryptoProvider;
use matrix_dmls::types::DmlsEpoch;
use openmls::prelude::*;
use openmls_rust_crypto::{MemoryKeyStore, MemoryKeyStoreError, OpenMlsRustCrypto};
use openmls_basic_credential::SignatureKeyPair;
use std::collections::hash_map::HashMap;
use std::vec::Vec;
use tokio;

#[derive(Debug)]
struct TestCryptoProvider {
    crypto_provider: OpenMlsRustCrypto,
    init_keys: HashMap<Vec<u8>, Vec<KeyPackage>>,
}

impl TestCryptoProvider {
    pub fn new() -> Self {
        TestCryptoProvider {
            crypto_provider: OpenMlsRustCrypto::default(),
            init_keys: HashMap::new(),
        }
    }

    pub fn add_init_key(&mut self, user: &[u8], key_package: KeyPackage) -> () {
        match self.init_keys.get_mut(user) {
            None => {
                self.init_keys
                    .insert(user.to_vec(), Vec::from([key_package]));
            }
            Some(key_packages) => {
                key_packages.push(key_package);
            }
        }
    }
}

impl DmlsCryptoProvider for TestCryptoProvider {
    type KeyStoreError = MemoryKeyStoreError;
    type OpenMlsKeyStore = MemoryKeyStore;
    type OpenMlsCryptoProvider = OpenMlsRustCrypto;
    fn mls_crypto_provider(&mut self) -> &mut Self::OpenMlsCryptoProvider {
        &mut self.crypto_provider
    }
    async fn get_init_keys(&mut self, users: &[&[u8]]) -> Vec<Option<KeyPackageIn>> {
        Vec::from_iter(users.iter().map(|user| {
            self.init_keys
                .get_mut(&user.to_vec())
                .and_then(|key_packages| key_packages.pop().map(|kp| kp.into()))
        }))
    }
}

// The following two helpers were taken from https://docs.rs/openmls/latest/openmls/

// A helper to create and store credentials.
fn generate_credential_with_key(
    backend: &impl OpenMlsCryptoProvider,
    identity: Vec<u8>,
    credential_type: CredentialType,
    signature_algorithm: SignatureScheme,
) -> (CredentialWithKey, SignatureKeyPair) {
    let credential = Credential::new(identity, credential_type).unwrap();
    let signature_keys =
        SignatureKeyPair::new(signature_algorithm)
            .expect("Error generating a signature key pair.");

    // Store the signature key into the key store so OpenMLS has access
    // to it.
    signature_keys
        .store(backend.key_store())
        .expect("Error storing signature keys in key store.");

    (
        CredentialWithKey {
            credential,
            signature_key: signature_keys.public().into(),
        },
        signature_keys,
    )
}

// A helper to create key package bundles.
fn generate_key_package(
    backend: &impl OpenMlsCryptoProvider,
    ciphersuite: Ciphersuite,
    credential_with_key: CredentialWithKey,
    signer: &SignatureKeyPair,
) -> KeyPackage {
    // Create the key package
    KeyPackage::builder()
        .build(
            CryptoConfig {
                ciphersuite,
                version: ProtocolVersion::default(),
            },
            backend,
            signer,
            credential_with_key,
        )
        .unwrap()
}

fn mls_message_to_protocol_message(msg: MlsMessageIn) -> ProtocolMessage {
    match msg.extract() {
        MlsMessageInBody::PublicMessage(msg) => msg.into(),
        MlsMessageInBody::PrivateMessage(msg) => msg.into(),
        _ => panic!("Unexpected message type"),
    }
}

#[tokio::test]
async fn test_create_group() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;

    let mut alice_backend = TestCryptoProvider::new();

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &MlsGroupConfig::default(),
        GroupId::from_slice(b"group"),
        alice_credential_with_key.clone(),
    )
    .expect("An unexpected error occurred");

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let bob_key_package = generate_key_package(
        bob_backend.mls_crypto_provider(),
        ciphersuite,
        bob_credential_with_key.clone(),
        &bob_signer,
    );

    alice_backend.add_init_key("bob".as_bytes(), bob_key_package);
    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    let (add_message_out, _, _, welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Bob");
    let (welcome, ratchet_tree, added) = welcome_info.expect("No welcome information");

    let mut bob_group = DmlsGroup::new_from_welcome(
        &mut bob_backend,
        &MlsGroupConfig::default(),
        welcome,
        Some(ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    let (message_out, _) = alice_group
        .encrypt_message(&mut alice_backend, &alice_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    let message_in = MlsMessageIn::tls_deserialize(
        &mut message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
        .expect("error deserializing");

    let processed_message = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(message_in),
            "alice".as_bytes(),
        )
        .expect("error processing");

    match processed_message.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }
}

#[tokio::test]
async fn test_basic_merge_epoch() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;

    let mut alice_backend = TestCryptoProvider::new();

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &MlsGroupConfig::default(),
        GroupId::from_slice(b"group"),
        alice_credential_with_key.clone(),
    )
    .expect("An unexpected error occurred");

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let bob_key_package = generate_key_package(
        bob_backend.mls_crypto_provider(),
        ciphersuite,
        bob_credential_with_key.clone(),
        &bob_signer,
    );

    alice_backend.add_init_key("bob".as_bytes(), bob_key_package);
    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    let (_add_message_out, _, _, welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Bob");
    let (welcome, ratchet_tree, added) = welcome_info.expect("No welcome information");

    let mut bob_group = DmlsGroup::new_from_welcome(
        &mut bob_backend,
        &MlsGroupConfig::default(),
        welcome,
        Some(ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    let (alice_update_message_out, alice_update_from_epoch, alice_update_resolves, _) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Alice could not create update");

    let (bob_update_message_out, bob_update_from_epoch, bob_update_resolves, _) = bob_group
        .resolve(&mut bob_backend, &bob_signer)
        .await
        .expect("Bob could not create update");

    let alice_update_message_in = MlsMessageIn::tls_deserialize(
        &mut alice_update_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
    .expect("error deserializing");

    let alice_update_message_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(alice_update_message_in),
            "alice".as_bytes(),
        )
        .expect("error processing");


    match alice_update_message_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            bob_group.merge_staged_commit(
                &mut bob_backend,
                *commit,
                &alice_update_from_epoch,
                "alice".as_bytes(),
                alice_update_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    let bob_update_message_in = MlsMessageIn::tls_deserialize(
        &mut bob_update_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
    .expect("error deserializing");

    let bob_update_message_processed = alice_group
        .process_message(
            &mut alice_backend,
            mls_message_to_protocol_message(bob_update_message_in),
            "alice".as_bytes(),
        )
        .expect("error parsing");

    match bob_update_message_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            alice_group.merge_staged_commit(
                &mut alice_backend,
                *commit,
                &bob_update_from_epoch,
                "bob".as_bytes(),
                bob_update_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    let (alice_merge_message_out, alice_merge_from_epoch, alice_merge_resolves, _) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Alice could not create merge");

    assert_eq!(alice_merge_resolves.len(), 1);

    let alice_merge_message_in = MlsMessageIn::tls_deserialize(
        &mut alice_merge_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
    .expect("error deserializing");

    let alice_merge_message_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(alice_merge_message_in),
            alice_merge_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match alice_merge_message_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            bob_group.merge_staged_commit(
                &mut bob_backend,
                *commit,
                &alice_merge_from_epoch,
                "alice".as_bytes(),
                alice_merge_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    let (message_out, message_epoch) = alice_group
        .encrypt_message(&mut alice_backend, &alice_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    let message_in =
        MlsMessageIn::tls_deserialize(
            &mut message_out.tls_serialize_detached().unwrap().as_slice()
        ).unwrap();

    let processed_message = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(message_in),
            message_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match processed_message.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }

    let bob_merged_group = bob_group.group(&mut bob_backend).expect("Not merged");
    let bob_members = bob_merged_group.leaf_nodes().collect::<Vec<_>>();
    // Alice updates twice, so has generation 2.  Bob updates once, so has generation 1
    assert_eq!(get_generation(bob_members[0].unwrap()), 2);
    assert_eq!(get_generation(bob_members[1].unwrap()), 1);
}

#[tokio::test]
async fn test_merge_epoch_with_adds_and_removes() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;

    let mut alice_backend = TestCryptoProvider::new();

    // Alice creates the group

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &MlsGroupConfig::default(),
        GroupId::from_slice(b"group"),
        alice_credential_with_key.clone(),
    )
    .expect("An unexpected error occurred");

    // Alice invites Bob

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let bob_key_package = generate_key_package(
        bob_backend.mls_crypto_provider(),
        ciphersuite,
        bob_credential_with_key.clone(),
        &bob_signer,
    );

    alice_backend.add_init_key("bob".as_bytes(), bob_key_package);
    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    let (add_bob_message_out, _, _, bob_welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Bob");
    let (bob_welcome, bob_ratchet_tree, _) = bob_welcome_info.expect("No welcome information");

    let mut bob_group = DmlsGroup::new_from_welcome(
        &mut bob_backend,
        &MlsGroupConfig::default(),
        bob_welcome,
        Some(bob_ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    // Bob invites Carol

    let mut carol_backend = TestCryptoProvider::new();

    let (carol_credential_with_key, carol_signer) = generate_credential_with_key(
        carol_backend.mls_crypto_provider(),
        Vec::from("carol"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let carol_key_package = generate_key_package(
        carol_backend.mls_crypto_provider(),
        ciphersuite,
        carol_credential_with_key.clone(),
        &carol_signer,
    );

    bob_backend.add_init_key("carol".as_bytes(), carol_key_package);
    bob_group.add_member(&mut bob_backend, "carol".as_bytes());
    let (add_carol_message_out, add_carol_epoch, add_carol_resolves, carol_welcome_info) =
        bob_group
            .resolve(&mut bob_backend, &bob_signer)
            .await
            .expect("Could not add Carol");
    let (carol_welcome, carol_ratchet_tree, _) =
        carol_welcome_info.expect("No welcome information");

    let mut carol_group = DmlsGroup::new_from_welcome(
        &mut carol_backend,
        &MlsGroupConfig::default(),
        carol_welcome,
        Some(carol_ratchet_tree.into()),
        "bob".as_bytes(),
    )
    .expect("An unexpected error occurred");

    // Meanwhile, Alice adds Dan, and then Dan removes Alice

    let mut dan_backend = TestCryptoProvider::new();

    let (dan_credential_with_key, dan_signer) = generate_credential_with_key(
        dan_backend.mls_crypto_provider(),
        Vec::from("dan"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let dan_key_package = generate_key_package(
        dan_backend.mls_crypto_provider(),
        ciphersuite,
        dan_credential_with_key.clone(),
        &dan_signer,
    );

    alice_backend.add_init_key("dan".as_bytes(), dan_key_package);
    alice_group.add_member(&mut alice_backend, "dan".as_bytes());
    let (add_dan_message_out, add_dan_from_epoch, add_dan_resolves, dan_welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Dan");
    let (dan_welcome, dan_ratchet_tree, _) = dan_welcome_info.expect("No welcome information");

    let mut dan_group = DmlsGroup::new_from_welcome(
        &mut dan_backend,
        &MlsGroupConfig::default(),
        dan_welcome,
        Some(dan_ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    dan_group.remove_member(&mut dan_backend, "alice".as_bytes());
    let (remove_alice_message_out, remove_alice_from_epoch, remove_alice_resolves, _) = dan_group
        .resolve(&mut dan_backend, &dan_signer)
        .await
        .expect("Could not remove Alice");

    // send Dan's state to Bob

    let add_dan_message_in = MlsMessageIn::tls_deserialize(
        &mut add_dan_message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice(),
    )
        .unwrap();

    let add_dan_message_bob_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(add_dan_message_in),
            add_dan_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match add_dan_message_bob_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            bob_group.merge_staged_commit(
                &mut bob_backend,
                *commit,
                &add_dan_from_epoch,
                "alice".as_bytes(),
                add_dan_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    let remove_alice_message_in = MlsMessageIn::tls_deserialize(
        &mut remove_alice_message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice(),
    )
        .unwrap();

    let remove_alice_message_bob_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(remove_alice_message_in),
            remove_alice_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match remove_alice_message_bob_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            bob_group.merge_staged_commit(
                &mut bob_backend,
                *commit,
                &remove_alice_from_epoch,
                "dan".as_bytes(),
                remove_alice_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    // Bob updates to latest group membership

    bob_group.add_member(&mut bob_backend, "dan".as_bytes());
    bob_group.remove_member(&mut bob_backend, "alice".as_bytes());
    assert!(bob_group.has_changes());
    assert!(bob_group.needs_resolve());

    // Bob merges the branch
    let (merge_message_out, merge_from_epoch, merge_resolves, merge_welcome_info) = bob_group
        .resolve(&mut bob_backend, &bob_signer)
        .await
        .expect("Bob could not merge");

    assert_eq!(merge_resolves.len(), 1);
    assert!(!bob_group.needs_resolve());

    let (merge_welcome, merge_ratchet_tree, merge_added) =
        merge_welcome_info.expect("No welcome information");

    assert_eq!(merge_added.len(), 1);

    // send Bob's state to Dan

    let merge_message_in = MlsMessageIn::tls_deserialize(
        &mut merge_message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice()
    )
        .unwrap();

    let merge_message_dan_processed = dan_group
        .process_message(
            &mut dan_backend,
            mls_message_to_protocol_message(merge_message_in),
            merge_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match merge_message_dan_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            dan_group.merge_staged_commit(
                &mut dan_backend,
                *commit,
                &merge_from_epoch,
                "bob".as_bytes(),
                merge_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    // send Bob's state to Carol - this will be via a welcome package because
    // the epoch was based on Dan's commit, which doesn't have Carol in it

    let new_carol_group = DmlsGroup::new_from_welcome(
        &mut carol_backend,
        &MlsGroupConfig::default(),
        merge_welcome,
        Some(merge_ratchet_tree.into()),
        "bob".as_bytes(),
    )
    .expect("An unexpected error occurred");
    carol_group
        .add_epoch_from_new_group(&mut carol_backend, new_carol_group, &merge_resolves)
        .expect("Could not add epoch");
    // the current group should be the new base group, and no extremities need
    // to be merged
    assert!(!carol_group.needs_resolve());
    // carol still has the old membership, so the group should think there are
    // membership changes
    assert!(carol_group.has_changes());

    // Carol updates to latest group membership

    carol_group.add_member(&mut carol_backend, "dan".as_bytes());
    carol_group.remove_member(&mut carol_backend, "alice".as_bytes());
    assert!(!carol_group.has_changes());

    // Bob sends a message

    let (message_out, message_epoch) = bob_group
        .encrypt_message(&mut bob_backend, &bob_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    let message_in = MlsMessageIn::tls_deserialize(
        &mut message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice()
    )
        .unwrap();

    let carol_processed_message = carol_group
        .process_message(
            &mut carol_backend,
            mls_message_to_protocol_message(message_in.clone()),
            message_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match carol_processed_message.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }

    let dan_processed_message = dan_group
        .process_message(
            &mut dan_backend,
            mls_message_to_protocol_message(message_in),
            message_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match dan_processed_message.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }
}

#[tokio::test]
async fn test_add_and_remove_record_keeping() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;

    let mut alice_backend = TestCryptoProvider::new();

    // Alice creates the group

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &MlsGroupConfig::default(),
        GroupId::from_slice(b"group"),
        alice_credential_with_key.clone(),
    )
    .expect("An unexpected error occurred");

    // Alice adds Bob

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let bob_key_package = generate_key_package(
        bob_backend.mls_crypto_provider(),
        ciphersuite,
        bob_credential_with_key.clone(),
        &bob_signer,
    );

    alice_backend.add_init_key("bob".as_bytes(), bob_key_package);
    // if a member is added and then removed without creating a new commit in
    // between, then it shouldn't require a new commit
    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    assert!(alice_group.has_changes());

    alice_group.remove_member(&mut alice_backend, "bob".as_bytes());
    assert!(!alice_group.has_changes());

    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    assert!(alice_group.has_changes());

    let (add_bob_message_out, _, _, bob_welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Bob");
    let (bob_welcome, bob_ratchet_tree, _) = bob_welcome_info.expect("No welcome information");

    let mut bob_group = DmlsGroup::new_from_welcome(
        &mut bob_backend,
        &MlsGroupConfig::default(),
        bob_welcome,
        Some(bob_ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    // removing a person who isn't in the group shouldn't cause any changes
    bob_group.remove_member(&mut bob_backend, "carol".as_bytes());
    assert!(!bob_group.has_changes());

    // adding a person who's already in the group shouldn't cause any changes
    bob_group.add_member(&mut bob_backend, "alice".as_bytes());
    assert!(!bob_group.has_changes());

    // Bob adds Carol

    let mut carol_backend = TestCryptoProvider::new();

    let (carol_credential_with_key, carol_signer) = generate_credential_with_key(
        carol_backend.mls_crypto_provider(),
        Vec::from("carol"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let carol_key_package = generate_key_package(
        carol_backend.mls_crypto_provider(),
        ciphersuite,
        carol_credential_with_key.clone(),
        &carol_signer,
    );

    bob_backend.add_init_key("carol".as_bytes(), carol_key_package);
    bob_group.add_member(&mut bob_backend, "carol".as_bytes());
    let (add_carol_message_out, add_carol_from_epoch, add_carol_resolves, carol_welcome_info) = bob_group
        .resolve(&mut bob_backend, &bob_signer)
        .await
        .expect("Could not add Bob");
    let (carol_welcome, carol_ratchet_tree, _) = carol_welcome_info.expect("No welcome information");

    let mut carol_group = DmlsGroup::new_from_welcome(
        &mut carol_backend,
        &MlsGroupConfig::default(),
        carol_welcome,
        Some(carol_ratchet_tree.into()),
        "bob".as_bytes(),
    )
        .expect("An unexpected error occurred");

    let add_carol_message_in = MlsMessageIn::tls_deserialize(
        &mut add_carol_message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice()
    )
        .unwrap();
    let add_carol_message_alice_processed = alice_group
        .process_message(
            &mut alice_backend,
            mls_message_to_protocol_message(add_carol_message_in),
            add_carol_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match add_carol_message_alice_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            alice_group.merge_staged_commit(
                &mut alice_backend,
                *commit,
                &add_carol_from_epoch,
                "alice".as_bytes(),
                add_carol_resolves.as_slice(),
            );
        }
        _ => panic!("Unexpected message type"),
    }

    assert!(alice_group.has_changes());
    alice_group.add_member(&mut alice_backend, "carol".as_bytes());
    assert!(!alice_group.has_changes());
}

#[tokio::test]
async fn test_external_commit() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
    let config = MlsGroupConfigBuilder::new()
        .wire_format_policy(MIXED_PLAINTEXT_WIRE_FORMAT_POLICY)
        .build();

    let mut alice_backend = TestCryptoProvider::new();

    // Alice creates the group

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &config,
        GroupId::from_slice(b"group"),
        alice_credential_with_key.clone(),
    )
        .expect("An unexpected error occurred");

    let public_group_info = alice_group.public_group_info(
        &mut alice_backend,
        &alice_signer,
        true,
    )
        .expect("Unable to get public group info");

    // Bob joins by external commit

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let serialized_group_info = public_group_info.tls_serialize_detached()
        .expect("Unable to serialize public group state");

    let group_info_message_in = MlsMessageIn::tls_deserialize(
        &mut serialized_group_info.as_slice()
    )
        .expect("error deserializing");

    let verifiable_group_info =
        if let MlsMessageInBody::GroupInfo(group_info) = group_info_message_in.extract() {
            group_info
        } else {
            panic!("Unexpected message type")
        };

    let (mut bob_group, bob_join_message_out) = DmlsGroup::join_by_external_commit(
        &mut bob_backend,
        &bob_signer,
        None,
        verifiable_group_info,
        &config,
        &[],
        bob_credential_with_key,
    ).expect("Could not join");

    let bob_join_from_epoch = DmlsEpoch::new(
        alice_group.group(&mut alice_backend).unwrap().epoch(),
        "alice".as_bytes(),
    );

    // Bob invites Carol
    let mut carol_backend = TestCryptoProvider::new();

    let (carol_credential_with_key, carol_signer) = generate_credential_with_key(
        carol_backend.mls_crypto_provider(),
        Vec::from("carol"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let carol_key_package = generate_key_package(
        carol_backend.mls_crypto_provider(),
        ciphersuite,
        carol_credential_with_key,
        &carol_signer,
    );

    bob_backend.add_init_key("carol".as_bytes(), carol_key_package);
    bob_group.add_member(&mut bob_backend, "carol".as_bytes());
    let (add_carol_message_out, add_carol_from_epoch, add_carol_resolves, carol_welcome_info) =
        bob_group
            .resolve(&mut bob_backend, &bob_signer)
            .await
            .expect("Could not add Carol");
    let (carol_welcome, carol_ratchet_tree, _) =
        carol_welcome_info.expect("No welcome information");

    let mut carol_group = DmlsGroup::new_from_welcome(
        &mut carol_backend,
        &config,
        carol_welcome,
        Some(carol_ratchet_tree.into()),
        "bob".as_bytes(),
    )
        .expect("An unexpected error occurred");

    let (bob_hello_message_out, _) = bob_group
        .encrypt_message(&mut bob_backend, &bob_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    let bob_hello_message_in = MlsMessageIn::tls_deserialize(
        &mut bob_hello_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
        .expect("error deserializing");

    // Meanwhile, Alice invites Dan

    let mut dan_backend = TestCryptoProvider::new();

    let (dan_credential_with_key, dan_signer) = generate_credential_with_key(
        dan_backend.mls_crypto_provider(),
        Vec::from("dan"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let dan_key_package = generate_key_package(
        dan_backend.mls_crypto_provider(),
        ciphersuite,
        dan_credential_with_key,
        &dan_signer,
    );
    let dan_key_package_hash = dan_key_package
        .hash_ref(dan_backend.mls_crypto_provider().crypto())
        .expect("Could not hash KeyPackage");

    alice_backend.add_init_key("dan".as_bytes(), dan_key_package);
    alice_group.add_member(&mut alice_backend, "dan".as_bytes());
    let (add_dan_message_out, add_dan_from_epoch, add_dan_resolves, dan_welcome_info) =
        alice_group
            .resolve(&mut alice_backend, &alice_signer)
            .await
            .expect("Could not add Dan");
    let (dan_welcome, dan_ratchet_tree, _) =
        dan_welcome_info.expect("No welcome information");

    let mut dan_group = DmlsGroup::new_from_welcome(
        &mut dan_backend,
        &config,
        dan_welcome,
        Some(dan_ratchet_tree.into()),
        "alice".as_bytes(),
    )
        .expect("An unexpected error occurred");

    // send Bob's state to Alice

    let bob_join_message_in = MlsMessageIn::tls_deserialize(
        &mut bob_join_message_out
            .to_bytes()
            .unwrap()
            .as_slice()
    )
            .unwrap();

    let bob_join_message_alice_processed = alice_group
        .process_message(
            &mut alice_backend,
            mls_message_to_protocol_message(bob_join_message_in),
            &bob_join_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match bob_join_message_alice_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            alice_group.merge_staged_commit(
                &mut alice_backend,
                *commit,
                &bob_join_from_epoch,
                "bob".as_bytes(),
                &[],
            );
        },
        _ => panic!("Unexpected message type"),
    }

    let add_carol_message_in = MlsMessageIn::tls_deserialize(
        &mut add_carol_message_out
            .to_bytes()
            .unwrap()
            .as_slice()
    )
            .unwrap();

    let add_carol_message_alice_processed = alice_group
        .process_message(
            &mut alice_backend,
            mls_message_to_protocol_message(add_carol_message_in),
            &add_carol_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match add_carol_message_alice_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            alice_group.merge_staged_commit(
                &mut alice_backend,
                *commit,
                &add_carol_from_epoch,
                "bob".as_bytes(),
                &add_carol_resolves,
            );
        },
        _ => panic!("Unexpected message type"),
    }

    let bob_hello_message_alice_processed = alice_group
        .process_message(
            &mut alice_backend,
            mls_message_to_protocol_message(bob_hello_message_in),
            "bob".as_bytes(),
        )
        .expect("error parsing");

    match bob_hello_message_alice_processed.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }

    // Alice resolves

    assert!(alice_group.needs_resolve());

    let (merge_message_out, merge_from_epoch, merge_resolves, merge_welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Bob could not merge");

    assert_eq!(merge_resolves.len(), 1);

    let (alice_hello_message_out, _) = alice_group
        .encrypt_message(&mut alice_backend, &alice_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    let alice_hello_message_in = MlsMessageIn::tls_deserialize(
        &mut alice_hello_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
    .expect("error deserializing");

    // send merge to Bob

    let merge_message_in = MlsMessageIn::tls_deserialize(
        &mut merge_message_out
            .tls_serialize_detached()
            .unwrap()
            .as_slice(),
    )
    .unwrap();

    let merge_message_bob_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(merge_message_in),
            &merge_from_epoch.creator().as_slice(),
        )
        .expect("error parsing");

    match merge_message_bob_processed.into_content() {
        ProcessedMessageContent::StagedCommitMessage(commit) => {
            bob_group.merge_staged_commit(
                &mut bob_backend,
                *commit,
                &merge_from_epoch,
                "alice".as_bytes(),
                &merge_resolves,
            );
        },
        _ => panic!("Unexpected message type"),
    }

    let alice_hello_message_bob_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(alice_hello_message_in),
            "alice".as_bytes(),
        )
        .expect("error parsing");

    match alice_hello_message_bob_processed.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }
}

#[tokio::test]
async fn test_export_and_import() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
    let config = MlsGroupConfigBuilder::new()
        .wire_format_policy(MIXED_PLAINTEXT_WIRE_FORMAT_POLICY)
        .build();

    let mut alice_backend = TestCryptoProvider::new();

    // Alice creates the group

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &config,
        GroupId::from_slice(b"group"),
        alice_credential_with_key,
    )
        .expect("An unexpected error occurred");

    let public_group_info = alice_group.public_group_info(
        &mut alice_backend,
        &alice_signer,
        true,
    )
        .expect("Unable to get public group state");

    let epoch = alice_group.epoch()
        .expect("Epoch not found")
        .clone();
    let export = alice_group.export_group(&mut alice_backend, &epoch)
        .expect("Could not export");

    let (alice_hello_message_out, _) = alice_group
        .encrypt_message(&mut alice_backend, &alice_signer, "Hello world!".as_bytes())
        .expect("Could not encrypt message");

    // Bob joins by external commit

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let serialized_group_info = public_group_info.tls_serialize_detached()
        .expect("Unable to serialize public group info");

    let group_info_message_in = MlsMessageIn::tls_deserialize(
        &mut serialized_group_info.as_slice()
    )
        .expect("error deserializing");

    let verifiable_group_info =
        if let MlsMessageInBody::GroupInfo(group_info) = group_info_message_in.extract() {
            group_info
        } else {
            panic!("Unexpected message type")
        };

    let (mut bob_group, bob_join_message_out) = DmlsGroup::join_by_external_commit(
        &mut bob_backend,
        &bob_signer,
        None,
        verifiable_group_info,
        &config,
        &[],
        bob_credential_with_key,
    ).expect("Could not join");

    bob_group.import_group(&mut bob_backend, &epoch, export)
        .expect("Could not import");

    let alice_hello_message_in = MlsMessageIn::tls_deserialize(
        &mut alice_hello_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
        .expect("error deserializing");

    let alice_hello_message_bob_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(alice_hello_message_in),
            "alice".as_bytes(),
        )
        .expect("error parsing");

    match alice_hello_message_bob_processed.into_content() {
        ProcessedMessageContent::ApplicationMessage(app_message) => {
            assert_eq!(
                app_message.into_bytes().as_slice(),
                "Hello world!".as_bytes()
            )
        }
        _ => panic!("Unexpected message type"),
    }
}

#[tokio::test]
async fn test_remove() {
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
    let config = MlsGroupConfigBuilder::new()
        .wire_format_policy(MIXED_PLAINTEXT_WIRE_FORMAT_POLICY)
        .build();

    let mut alice_backend = TestCryptoProvider::new();

    // Alice creates the group

    let (alice_credential_with_key, alice_signer) = generate_credential_with_key(
        alice_backend.mls_crypto_provider(),
        Vec::from("alice"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );

    let mut alice_group = DmlsGroup::new(
        &mut alice_backend,
        &alice_signer,
        &config,
        GroupId::from_slice(b"group"),
        alice_credential_with_key,
    )
        .expect("An unexpected error occurred");

    // alice adds bob

    let mut bob_backend = TestCryptoProvider::new();

    let (bob_credential_with_key, bob_signer) = generate_credential_with_key(
        bob_backend.mls_crypto_provider(),
        Vec::from("bob"),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
    );
    let bob_key_package = generate_key_package(
        bob_backend.mls_crypto_provider(),
        ciphersuite,
        bob_credential_with_key,
        &bob_signer,
    );

    alice_backend.add_init_key("bob".as_bytes(), bob_key_package);
    alice_group.add_member(&mut alice_backend, "bob".as_bytes());
    let (add_message_out, _, _, welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not add Bob");
    let (welcome, ratchet_tree, added) = welcome_info.expect("No welcome information");

    let mut bob_group = DmlsGroup::new_from_welcome(
        &mut bob_backend,
        &config,
        welcome,
        Some(ratchet_tree.into()),
        "alice".as_bytes(),
    )
    .expect("An unexpected error occurred");

    // Alice removes Bob

    alice_group.remove_member(&mut alice_backend, "bob".as_bytes());
    let (remove_message_out, remove_from_epoch, remove_resolves, welcome_info) = alice_group
        .resolve(&mut alice_backend, &alice_signer)
        .await
        .expect("Could not remove Bob");

    let remove_message_in = MlsMessageIn::tls_deserialize(
        &mut remove_message_out
            .tls_serialize_detached()
            .expect("Could not serialize")
            .as_slice(),
    )
    .expect("error deserializing");

    let remove_message_processed = bob_group
        .process_message(
            &mut bob_backend,
            mls_message_to_protocol_message(remove_message_in),
            "alice".as_bytes(),
        )
        .expect("error parsing");

    if let ProcessedMessageContent::StagedCommitMessage(commit) = remove_message_processed.into_content() {
        bob_group.merge_staged_commit(
            &mut bob_backend,
            *commit,
            &remove_from_epoch,
            "alice".as_bytes(),
            remove_resolves.as_slice(),
        );
    } else {
        panic!("Unexpected message type");
    }

    assert!(!bob_group.is_joined());
}
