Matrix DMLS
===========

Matrix DMLS is a wrapper around [OpenMLS](https://github.com/openmls/openmls)
that implements a decentralised version of MLS.

## Status

This library is still in development and not ready for use.

matrix-dmls is the library itself.  matrix-dmls-wasm is a wrapper for calling
matrix-dmls from wasm.
