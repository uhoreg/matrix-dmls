let assert = require('node:assert/strict');

Error.stackTraceLimit = 100;

let webcrypto = require('node:crypto');
globalThis.crypto = webcrypto.webcrypto;

let matrix_dmls = require("./pkg/matrix_dmls_wasm");

const { Buffer } = require('node:buffer');

class Storage {
    constructor() {
        this.storage = {};
        this.key_packages = {};
        this.get_init_keys = this.get_init_keys.bind(this);
    }

    add_init_key(user, key_package) {
        if (!this.key_packages[user]) {
            this.key_packages[user] = [];
        }
        this.key_packages[user].push(key_package);
    }

    async get_init_keys(users) {
        return users.map(user => this.key_packages[user] ? this.key_packages[user].pop() : undefined);
    }
}

//matrix_dmls.init_thread_pool(true);

const encoder = new TextEncoder();
const decoder = new TextDecoder();

(async () => {

await matrix_dmls.initAsync();

let aliceStorage = new Storage();
let aliceBackend = new matrix_dmls.DmlsCryptoProvider(
    aliceStorage.get_init_keys,
);

let aliceCredential = new matrix_dmls.CredentialWithKey(aliceBackend, encoder.encode("alice"));

let aliceGroup = new matrix_dmls.DmlsGroup(aliceBackend, aliceCredential, encoder.encode("group"));

console.log("Testing invite...");

let bobStorage = new Storage();
let bobBackend = new matrix_dmls.DmlsCryptoProvider(
    bobStorage.get_init_keys,
);

let bobCredential = new matrix_dmls.CredentialWithKey(bobBackend, encoder.encode("bob"));

let [kp] = bobBackend.make_init_keys(bobCredential, 1);

aliceStorage.add_init_key(encoder.encode("bob"), kp);

aliceGroup.add_member(aliceBackend, encoder.encode("bob"));

let serialized = aliceGroup.to_yaml();
// console.log(serialized);
aliceGroup = matrix_dmls.DmlsGroup.from_yaml(serialized);

let [commit, mls_epoch, creator, resolves, [welcome, adds]] = await aliceGroup.resolve(aliceBackend, aliceCredential);

let bobGroup = matrix_dmls.DmlsGroup.new_from_welcome(bobBackend, welcome, encoder.encode("alice"));

let [epochNum, epochCreator] = bobGroup.epoch();
let groupExport = aliceGroup.export_group(bobBackend, BigInt(epochNum), epochCreator);

let [ciphertext, message_mls_epoch, message_epoch_creator] = aliceGroup.encrypt_message(aliceBackend, aliceCredential, encoder.encode("Hello world"))

let message_in = (new matrix_dmls.MlsMessageIn(ciphertext)).as_protocol_message();

assert.ok(!message_in.is_handshake_message);

let processed = bobGroup.process_message(bobBackend, message_in, message_epoch_creator);

assert.ok(processed.is_application_message());
assert.equal(
    decoder.decode(Uint8Array.from(processed.as_application_message())),
    "Hello world",
);

console.log("Ok");

console.log("Testing external commit");

let public_group_info = bobGroup.public_group_info(bobBackend, bobCredential);

let carolStorage = new Storage();
let carolBackend = new matrix_dmls.DmlsCryptoProvider(
    carolStorage.get_init_keys,
);

let carolCredential = new matrix_dmls.CredentialWithKey(carolBackend, encoder.encode("carol"));

let carolJoinResult = matrix_dmls.DmlsGroup.join_by_external_commit(
    carolBackend,
    public_group_info,
    carolCredential,
);

let carolJoinMsg = carolJoinResult.message;
let carolGroup = carolJoinResult.group;
let carolJoinMsgIn = (new matrix_dmls.MlsMessageIn(carolJoinMsg)).as_protocol_message();
assert.ok(carolJoinMsgIn.is_handshake_message);

let carolJoinFromEpoch = carolJoinMsgIn.epoch;

let carolJoinBobProcessed = bobGroup.process_message(bobBackend, carolJoinMsgIn, message_epoch_creator);

assert.ok(carolJoinBobProcessed.is_staged_commit());
bobGroup.merge_staged_commit(
    carolJoinBobProcessed.as_staged_commit(),
    carolJoinFromEpoch,
    encoder.encode("alice"),
    encoder.encode("carol"),
    [],
    bobBackend,
);

carolJoinMsgIn = (new matrix_dmls.MlsMessageIn(carolJoinMsg)).as_protocol_message();

let carolJoinAliceProcessed = aliceGroup.process_message(aliceBackend, carolJoinMsgIn, message_epoch_creator);

assert.ok(carolJoinAliceProcessed.is_staged_commit());
aliceGroup.merge_staged_commit(
    carolJoinAliceProcessed.as_staged_commit(),
    carolJoinFromEpoch,
    encoder.encode("alice"),
    encoder.encode("carol"),
    [],
    aliceBackend,
);

let [carol_ciphertext, carol_message_mls_epoch, carol_message_epoch_creator] = carolGroup.encrypt_message(carolBackend, carolCredential, encoder.encode("Hello world"))

let carolMessageIn = (new matrix_dmls.MlsMessageIn(carol_ciphertext)).as_protocol_message();

let carolMsgBobProcessed = bobGroup.process_message(bobBackend, carolMessageIn, carol_message_epoch_creator);

assert.ok(carolMsgBobProcessed.is_application_message());
assert.equal(
    decoder.decode(Uint8Array.from(carolMsgBobProcessed.as_application_message())),
    "Hello world",
);

carolMessageIn = (new matrix_dmls.MlsMessageIn(carol_ciphertext)).as_protocol_message();

let carolMsgAliceProcessed = aliceGroup.process_message(aliceBackend, carolMessageIn, carol_message_epoch_creator);

assert.ok(carolMsgAliceProcessed.is_application_message());
assert.equal(
    decoder.decode(Uint8Array.from(carolMsgAliceProcessed.as_application_message())),
    "Hello world",
);

console.log("Ok");

console.log("Testing group import...");

carolGroup.import_group(carolBackend, BigInt(epochNum), epochCreator, groupExport);

message_in = (new matrix_dmls.MlsMessageIn(ciphertext)).as_protocol_message();
let aliceMsgCarolProcessed = carolGroup.process_message(carolBackend, message_in, message_epoch_creator);

assert.ok(aliceMsgCarolProcessed.is_application_message());
assert.equal(
    decoder.decode(Uint8Array.from(aliceMsgCarolProcessed.as_application_message())),
    "Hello world",
);

console.log("Ok");

})();
