// Copyright 2023 The Matrix.org Foundation C.I.C.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#![feature(async_fn_in_trait)]

use matrix_dmls::dmls_group::DmlsGroup;
use matrix_dmls::traits::DmlsCryptoProvider;
use matrix_dmls::types::DmlsEpoch;
use openmls::prelude::*;
use openmls_rust_crypto::OpenMlsRustCrypto;
use openmls::group::export::MlsGroupExport;
use openmls_basic_credential::SignatureKeyPair;
use openmls_traits::signatures::Signer;
use js_sys;
use serde_wasm_bindgen;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures;
use std::io::Cursor;
use std::vec::Vec;

//pub use wasm_bindgen_rayon::init_thread_pool;

/// Implements a [`matrix_dmls::traits::DmlsCryptoProvider`] using JavaScript
/// functions
#[wasm_bindgen(js_name=DmlsCryptoProvider)]
pub struct JsDmlsCryptoProvider {
    // FIXME: use a custom MLS crypto provider with own key storage
    pub(crate) crypto_provider: OpenMlsRustCrypto,
    pub(crate) get_init_keys: js_sys::Function,
}

#[wasm_bindgen(js_class=DmlsCryptoProvider)]
impl JsDmlsCryptoProvider {
    /// Creates a new crypto provider
    ///
    /// `get_init_keys` is an `async` function that takes an array of user IDs,
    /// where each user ID is an array of byte-sized integers, and returns an
    /// array of `undefined` (if no KeyPackage for the user could be found), or
    /// a KeyPackage in the form of an array of byte-sized integers or
    /// `Uint8Array`.
    #[wasm_bindgen(constructor)]
    pub fn new(
        get_init_keys: js_sys::Function,
    ) -> Self {
        JsDmlsCryptoProvider {
            crypto_provider: OpenMlsRustCrypto::default(),
            get_init_keys,
        }
    }

    /// Make some init key KeyPackages for the user
    ///
    /// `credential` is the credential to use for the KeyPackages, and `num` is
    /// the number of KeyPackages to create.
    pub fn make_init_keys(
        &mut self,
        credential_with_key: &JsCredentialWithKey,
        num: u16,
    ) -> Result<JsValue, String> {
        let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;

        let key_packages = (0..num).map(|_| {
            let key_package = KeyPackage::builder()
                .build(
                    CryptoConfig {
                        ciphersuite,
                        version: ProtocolVersion::default()
                    },
                    self.mls_crypto_provider(),
                    &credential_with_key.signer,
                    credential_with_key.credential_with_key.clone(),
                )
                .map_err(|e| format!("Unable to create KeyPackage: {}", e))?;

            key_package
                .tls_serialize_detached()
                .map_err(|e| e.to_string())
        }).collect::<Result<Vec<_>, String>>()?;

        serde_wasm_bindgen::to_value(&key_packages)
            .map_err(|e| e.to_string())
    }
}

impl DmlsCryptoProvider for JsDmlsCryptoProvider {
    type KeyStoreError = <<OpenMlsRustCrypto as openmls_traits::OpenMlsCryptoProvider>::KeyStoreProvider as OpenMlsKeyStore>::Error;
    type OpenMlsKeyStore = <OpenMlsRustCrypto as OpenMlsCryptoProvider>::KeyStoreProvider;
    type OpenMlsCryptoProvider = OpenMlsRustCrypto;
    fn mls_crypto_provider(&mut self) -> &mut Self::OpenMlsCryptoProvider {
        &mut self.crypto_provider
    }
    async fn get_init_keys(&mut self, users: &[&[u8]]) -> Vec<Option<KeyPackageIn>> {
        let js_promise = self.get_init_keys.call1(&JsValue::null(), &serde_wasm_bindgen::to_value(users).unwrap());
        match js_promise {
            Err(..) => Vec::new(),
            Ok(js_promise) => {
                let promise = js_sys::Promise::resolve(&js_promise);
                wasm_bindgen_futures::JsFuture::from(promise).await.ok()
                    .and_then(|val| serde_wasm_bindgen::from_value::<Vec<Option<Vec<u8>>>>(val).ok())
                    .map(|kps|
                         kps.iter()
                         .map(|maybe_kp| maybe_kp.as_ref().and_then(|kp| KeyPackageIn::tls_deserialize(&mut kp.as_slice()).ok()))
                        .collect::<Vec<_>>()
                    )
                    .unwrap_or(Vec::new()) // FIXME: make it the right size
            }
        }
    }
}

/// Wrapper around [`openmls::prelude::Credential`]
#[wasm_bindgen(js_name=Credential)]
pub struct JsCredential {
    credential: Credential,
}

#[wasm_bindgen(js_class=Credential)]
impl JsCredential {
    pub fn tls_serialize(&self) -> Result<Vec<u8>, String> {
        self.credential.tls_serialize_detached()
            .map_err(|e| e.to_string())
    }

    pub fn tls_deserialize(&self, cred: &[u8]) -> Result<JsCredential, String> {
        let credential = Credential::tls_deserialize(&mut Cursor::new(cred))
            .map_err(|e| e.to_string())?;
        Ok(Self {
            credential
        })
    }
}

#[wasm_bindgen(js_name=CredentialWithKey)]
pub struct JsCredentialWithKey {
    pub(crate) credential_with_key: CredentialWithKey,
    pub(crate) signer: SignatureKeyPair,
}

#[wasm_bindgen(js_class=CredentialWithKey)]
impl JsCredentialWithKey {
    /// Create a new credential with key for the given identity.
    ///
    /// The private key will be stored in the `backend`.
    #[wasm_bindgen(constructor)]
    pub fn new(backend: &mut JsDmlsCryptoProvider, identity: &[u8]) -> Self {
        let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
        let credential = Credential::new(identity.into(), CredentialType::Basic).unwrap();
        let signature_keys =
            SignatureKeyPair::new(ciphersuite.signature_algorithm())
                .expect("Error generating a signature key pair.");

        // Store the signature key into the key store so OpenMLS has access
        // to it.
        signature_keys
            .store(backend.mls_crypto_provider().key_store())
            .expect("Error storing signature keys in key store.");

        Self {
            credential_with_key: CredentialWithKey {
                credential,
                signature_key: signature_keys.public().into(),
            },
            signer: signature_keys,
        }
    }

    pub fn credential(&self) -> JsCredential {
        JsCredential {
            credential: self.credential_with_key.credential.clone(),
        }
    }

    /// Sign something with the given credential.
    pub fn sign(
        &mut self,
        backend: &JsDmlsCryptoProvider,
        payload: &[u8],
    ) -> Result<Vec<u8>, String> {
        self.signer.sign(payload)
            .or(Err("Unable to sign".to_string()))
    }

    // TODO: add a way to load from storage
}

/// Wrapper around [`MlsMessageIn`]
#[wasm_bindgen(js_name=MlsMessageIn)]
pub struct JsMlsMessageIn {
    pub(crate) message: MlsMessageIn,
}

#[wasm_bindgen(js_class=MlsMessageIn)]
impl JsMlsMessageIn {
    #[wasm_bindgen(constructor)]
    pub fn new(message: &[u8]) -> Result<JsMlsMessageIn, String> {
        let message = MlsMessageIn::tls_deserialize(&mut Cursor::new(message))
            .map_err(|e| e.to_string())?;
        Ok(JsMlsMessageIn { message })
    }

    pub fn as_protocol_message(self) -> Option<JsMlsProtocolMessage> {
        match self.message.extract() {
            MlsMessageInBody::PublicMessage(msg) => Some(JsMlsProtocolMessage { message: msg.into() }),
            MlsMessageInBody::PrivateMessage(msg) => Some(JsMlsProtocolMessage { message: msg.into() }),
            _ => None,
        }
    }
}

/// Wrapper around [`MlsProtocolMessage`]
#[wasm_bindgen(js_name=MlsProtocolMessage)]
pub struct JsMlsProtocolMessage {
    pub(crate) message: ProtocolMessage,
}

#[wasm_bindgen(js_class=MlsProtocolMessage)]
impl JsMlsProtocolMessage {
    #[wasm_bindgen(getter)]
    pub fn is_handshake_message(&self) -> bool {
        self.message.is_handshake_message()
    }

    #[wasm_bindgen(getter)]
    pub fn epoch(&self) -> u64 {
        self.message.epoch().as_u64()
    }

    #[wasm_bindgen(getter)]
    pub fn group_id(&self) -> Vec<u8> {
        self.message.group_id().to_vec()
    }
}

// TODO: add methods for inspecting message

/// Wrapper around [`openmls::prelude::StagedCommit`]
#[wasm_bindgen(js_name=StagedCommit)]
pub struct JsStagedCommit {
    commit: StagedCommit,
}

// TODO: add methods for inspecting commit

/// Wrapper around [`openmls::prelude::ProcessedMessage`]
#[wasm_bindgen(js_name=ProcessedMessage)]
pub struct JsProcessedMessage {
    message: ProcessedMessage,
}

#[wasm_bindgen(js_class=ProcessedMessage)]
impl JsProcessedMessage {
    pub fn is_application_message(&self) -> bool {
        match self.message.content() {
            ProcessedMessageContent::ApplicationMessage(..) => true,
            _ => false,
        }
    }

    pub fn as_application_message(self) -> Result<JsValue, String> {
        match self.message.into_content() {
            ProcessedMessageContent::ApplicationMessage(application_message) => {
                let bytes = application_message.into_bytes();
                serde_wasm_bindgen::to_value(&bytes)
                    .map_err(|e| e.to_string())
            },
            _ => Err("Not an application message".to_string()),
        }
    }

    pub fn is_staged_commit(&self) -> bool {
        match self.message.content() {
            ProcessedMessageContent::StagedCommitMessage(..) => true,
            _ => false,
        }
    }

    pub fn as_staged_commit(self) -> Result<JsStagedCommit, String> {
        match self.message.into_content() {
            ProcessedMessageContent::StagedCommitMessage(staged_commit) => {
                Ok(JsStagedCommit { commit: *staged_commit})
            },
            _ => Err("Not staged commit".to_string()),
        }
    }
}

/// Wrapper around `matrix_dmls::DmlsGroup`
#[wasm_bindgen(js_name=DmlsGroup)]
pub struct JsDmlsGroup {
    group: DmlsGroup,
}

/// The default MLS group config for testing
fn mls_group_config() -> MlsGroupConfig {
   MlsGroupConfig::builder()
        .use_ratchet_tree_extension(true)
        .wire_format_policy(MIXED_PLAINTEXT_WIRE_FORMAT_POLICY)
        .build()
}

#[wasm_bindgen(js_class=DmlsGroup)]
impl JsDmlsGroup {
    #[wasm_bindgen(constructor)]
    pub fn new(
        backend: &mut JsDmlsCryptoProvider,
        credential_with_key: &JsCredentialWithKey,
        group_id: &[u8],
    ) -> Result<JsDmlsGroup, String> {
        let group = DmlsGroup::new(
            backend,
            &credential_with_key.signer,
            &mls_group_config(),
            GroupId::from_slice(group_id),
            credential_with_key.credential_with_key.clone(),
        )
        .map_err(|e| e.to_string())?;

        Ok(Self {
            group,
        })
    }

    pub fn new_from_welcome(
        backend: &mut JsDmlsCryptoProvider,
        welcome: &[u8],
        sender: &[u8],
    ) -> Result<JsDmlsGroup, String> {
        let welcome_msg = Welcome::tls_deserialize(&mut Cursor::new(welcome))
            .map_err(|e| e.to_string())?;
        let group = DmlsGroup::new_from_welcome(
            backend,
            &mls_group_config(),
            welcome_msg,
            None,
            sender,
        )
        .map_err(|e| e.to_string())?;

        Ok(Self {
            group,
        })
    }

    pub fn join_by_external_commit(
        backend: &mut JsDmlsCryptoProvider,
        verifiable_group_info: &[u8],
        credential_with_key: &JsCredentialWithKey,
    ) -> Result<JsDmlsGroupWithMessage, String> {
        let message_in = MlsMessageIn::tls_deserialize(
            &mut Cursor::new(verifiable_group_info),
        ).map_err(|e| e.to_string())?;

        let verifiable_group_info = match message_in.extract() {
            MlsMessageInBody::GroupInfo(group_info) => Ok(group_info),
            _ => Err("Not group info"),
        }?;

        let (group, message) = DmlsGroup::join_by_external_commit(
            backend,
            &credential_with_key.signer,
            None,
            verifiable_group_info,
            &mls_group_config(),
            &[],
            credential_with_key.credential_with_key.clone(),
        ).map_err(|e| e.to_string())?;

        let msg_serialized = message
            .tls_serialize_detached()
            .expect("Error serializing signature key.");

        Ok(JsDmlsGroupWithMessage{
            group: Self {group},
            message: msg_serialized,
        })
    }

    pub fn new_dummy_group(
        backend: &mut JsDmlsCryptoProvider,
        group_id: &[u8],
        identity: &[u8],
    ) -> JsDmlsGroup {
        Self {
            group: DmlsGroup::new_dummy_group(
                backend,
                &mls_group_config(),
                GroupId::from_slice(group_id),
                identity,
            ),
        }
    }

    pub fn add_epoch_from_new_group(
        &mut self,
        backend: &mut JsDmlsCryptoProvider,
        other: JsDmlsGroup,
        resolves: JsValue,
    ) -> Result<(), String> {
        let resolves_vec = serde_wasm_bindgen::from_value::<Vec<(u64, Vec<u8>)>>(resolves)
            .map_err(|e| e.to_string())?
            .iter()
            .map(|(number, creator)| DmlsEpoch::new(GroupEpoch::from(*number), creator))
            .collect::<Vec<_>>();
        self.group.add_epoch_from_new_group(backend, other.group, resolves_vec.as_slice())
    }

    pub fn group_id(&self) -> Vec<u8> {
        self.group.group_id().to_vec()
    }

    pub fn process_message(
        &self,
        backend: &mut JsDmlsCryptoProvider,
        message: JsMlsProtocolMessage,
        epoch_creator: &[u8],
    ) -> Result<JsProcessedMessage, String> {
        let processed_message = self.group.process_message(backend, message.message, epoch_creator)
            .map_err(|e| e.to_string())?;
        Ok(JsProcessedMessage { message: processed_message })
    }

    pub fn merge_staged_commit(
        &mut self,
        staged_commit: JsStagedCommit,
        from_epoch_number: u64,
        from_epoch_creator: &[u8],
        sender: &[u8],
        resolves: JsValue,
        backend: &mut JsDmlsCryptoProvider,
    ) -> Result<JsValue, String> {
        let from_epoch = DmlsEpoch::new(GroupEpoch::from(from_epoch_number), from_epoch_creator);
        let resolves_vec = serde_wasm_bindgen::from_value::<Vec<(u64, Vec<u8>)>>(resolves)
            .map_err(|e| e.to_string())?
            .iter()
            .map(|(number, creator)| DmlsEpoch::new(GroupEpoch::from(*number), creator))
            .collect::<Vec<_>>();
        self.group.merge_staged_commit(
            backend,
            staged_commit.commit,
            &from_epoch,
            sender,
            resolves_vec.as_slice(),
        )
            .map_or(
                Err("Unable to merge commit".to_string()),
                |epoch| Ok(serde_wasm_bindgen::to_value(&(&epoch.mls_epoch().as_u64(),
                epoch.creator())).unwrap()),
            )
    }

    pub fn add_member(
        &mut self,
        backend: &mut JsDmlsCryptoProvider,
        member: &[u8],
    ) -> () {
        self.group.add_member(backend, member)
    }

    pub fn remove_member(
        &mut self,
        backend: &mut JsDmlsCryptoProvider,
        member: &[u8],
    ) -> () {
        self.group.remove_member(backend, member)
    }

    pub async fn resolve(
        &mut self,
        backend: &mut JsDmlsCryptoProvider,
        credential_with_key: &JsCredentialWithKey,
    ) -> Result<JsValue, String> {
        match self.group.resolve(backend, &credential_with_key.signer).await {
            Ok((msg_out, dmls_epoch, resolves, welcome_info)) => {
                let msg = msg_out.to_bytes()
                    .map_err(|e| e.to_string())?;
                let resolves_vec = resolves.iter().map(|epoch| (epoch.mls_epoch().as_u64(), epoch.creator()))
                    .collect::<Vec<_>>();
                let welcome_info = welcome_info.map(|(welcome, _, adds)| {
                    (welcome.tls_serialize_detached().unwrap(), adds)
                });
                Ok(serde_wasm_bindgen::to_value(&(
                    msg,
                    dmls_epoch.mls_epoch().as_u64(),
                    dmls_epoch.creator(),
                    resolves_vec,
                    welcome_info,
                )).unwrap())
            },
            Err(..) => {
                Err("Could not resolve".to_string())
            }
        }
    }

    pub fn encrypt_message(
        &mut self,
        backend: &mut JsDmlsCryptoProvider,
        credential_with_key: &JsCredentialWithKey,
        message: &[u8],
    ) -> Result<JsValue, String> {
        let (mls_message, dmls_epoch) = self.group.encrypt_message(
            backend,
            &credential_with_key.signer,
            message,
        )
            .map_err(|e| e.to_string())?;
        let msg = mls_message.to_bytes()
            .map_err(|e| e.to_string())?;
        Ok(serde_wasm_bindgen::to_value(&(
            msg,
            dmls_epoch.mls_epoch().as_u64(),
            dmls_epoch.creator(),
        )).unwrap())
    }

    pub fn public_group_info(
        &self,
        backend: &mut JsDmlsCryptoProvider,
        credential_with_key: &JsCredentialWithKey,
    ) -> Result<Vec<u8>, String> {
        let public_group_info = self.group.public_group_info(
            backend,
            &credential_with_key.signer,
            true,
        )
            .map_err(|e| e.to_string())?;
        public_group_info.tls_serialize_detached()
            .map_err(|e| e.to_string())
    }

    pub fn needs_resolve(&self) -> bool {
        self.group.needs_resolve()
    }

    pub fn has_changes(&self) -> bool {
        self.group.has_changes()
    }

    pub fn members(&self, backend: &mut JsDmlsCryptoProvider) -> JsValue {
        let members = self.group.members(backend);
        serde_wasm_bindgen::to_value(&Vec::from_iter(members.iter())).unwrap()
    }

    pub fn epoch(&self) -> Result<JsValue, String> {
        let epoch = self.group.epoch()
            .or(Err("Could not find epoch"))?;
        Ok(serde_wasm_bindgen::to_value(&(&epoch.mls_epoch().as_u64(),
                epoch.creator())).unwrap())
    }

    pub fn export_group(
        &self,
        backend: &mut JsDmlsCryptoProvider,
        epoch_number: u64,
        epoch_creator: &[u8],
    ) -> Result<Vec<u8>, String> {
        let export = self.group.export_group(backend, &DmlsEpoch::new(GroupEpoch::from(epoch_number), epoch_creator))
            .or(Err("Could not get export"))?;
        export.tls_serialize_detached()
            .map_err(|e| e.to_string())
    }

    pub fn import_group(
        &self,
        backend: &mut JsDmlsCryptoProvider,
        epoch_number: u64,
        epoch_creator: &[u8],
        export: Vec<u8>,
    ) -> Result<(), String> {
        let export = MlsGroupExport::tls_deserialize(&mut export.as_slice())
            .map_err(|e| e.to_string())?;
        self.group.import_group(
            backend,
            &DmlsEpoch::new(GroupEpoch::from(epoch_number), epoch_creator),
            export,
        ).or(Err("Could not import".to_string()))
    }

    pub fn is_joined(&self) -> bool {
        self.group.is_joined()
    }

    // we use yaml because json isn't happy about integer values being used as
    // keys
    pub fn to_yaml(&self) -> String {
        serde_yaml::to_string(&self.group).unwrap()
    }

    pub fn from_yaml(yaml: String) -> Result<JsDmlsGroup, String> {
        serde_yaml::from_str(&yaml)
            .map_or_else(
                |e| Err(e.to_string()),
                |group| Ok(JsDmlsGroup { group }),
            )
    }
}

#[wasm_bindgen]
pub struct JsDmlsGroupWithMessage{
    group: JsDmlsGroup,
    message: Vec<u8>,
}

#[wasm_bindgen]
impl JsDmlsGroupWithMessage {
    #[wasm_bindgen(getter)]
    pub fn group(self) -> JsDmlsGroup {
        self.group
    }

    #[wasm_bindgen(getter)]
    pub fn message(&self) -> Vec<u8> {
        self.message.clone()
    }
}

/// Run some stuff when the Wasm module is instantiated.
///
/// Right now, it does the following:
///
/// * Redirect Rust panics to JavaScript console.
#[wasm_bindgen(start)]
pub fn start() {
    console_error_panic_hook::set_once();
}
